package com.briup.visual.dao.extend;

import com.briup.visual.bean.Epidemic;

import java.util.List;

public interface EpidemicExtendMapper {
    List<Epidemic> query(int page, int pageSize, String province, String city, String area, String inputTime);

    long count(String province, String city, String area, String inputTime);

    List<Epidemic> queryProvinceDataByCountry(String country, String inputTime);
}


