package com.briup.visual.dao.extend;

import com.briup.visual.bean.Hospital;

import java.util.List;

public interface HospitalExtendMapper {
    List<Hospital> query(int page, int pageSize, String name);

    long count(String paramString);
}


