package com.briup.visual.bean.extend;

import com.briup.visual.bean.BasePrivilege;
import com.briup.visual.bean.BaseRole;

import java.util.List;


public class BaseRoleExtend
        extends BaseRole {
    private static final long serialVersionUID = 1L;
    private List<BasePrivilege> privileges;

    public List<BasePrivilege> getPrivileges() {
        return this.privileges;
    }

    public void setPrivileges(List<BasePrivilege> privileges) {
        this.privileges = privileges;
    }
}


