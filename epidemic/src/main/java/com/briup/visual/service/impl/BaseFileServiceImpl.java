package com.briup.visual.service.impl;

import com.briup.visual.bean.BaseFile;
import com.briup.visual.bean.BaseFileExample;
import com.briup.visual.dao.BaseFileMapper;
import com.briup.visual.service.IBaseFileService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BaseFileServiceImpl
        implements IBaseFileService {
    @Autowired
    private BaseFileMapper baseFileMapper;

    public void save(BaseFile baseFile) throws Exception {
        System.out.println("上传：" + baseFile.getFileName());
        this.baseFileMapper.insert(baseFile);
    }


    public void deleteById(String id) throws Exception {
        this.baseFileMapper.deleteByPrimaryKey(id);
    }


    public List<BaseFile> findAll() {
        return this.baseFileMapper.selectByExample(new BaseFileExample());
    }
}


