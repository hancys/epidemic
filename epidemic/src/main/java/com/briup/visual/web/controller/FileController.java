package com.briup.visual.web.controller;

import com.briup.visual.bean.BaseFile;
import com.briup.visual.jacky.FastDFS;
import com.briup.visual.jacky.Message;
import com.briup.visual.jacky.MessageUtil;
import com.briup.visual.service.IBaseFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@Api(description = "文件上传相关接口")
@RestController
@RequestMapping({"/file"})
public class FileController {
    @Autowired
    private IBaseFileService baseFileService;

    private void downLoadFromUrl(String urlStr, OutputStream os) {
        try {
            URL url = null;
            HttpURLConnection conn = null;
            InputStream inputStream = null;
            try {
                url = new URL(urlStr);
                conn = (HttpURLConnection) url.openConnection();

                conn.setConnectTimeout(3000);

                conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

                inputStream = conn.getInputStream();

                byte[] getData = readInputStream(inputStream);

                os.write(getData);
                os.flush();
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (os != null) {
                    os.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }


    @ApiOperation(value = "下载文件", produces = "application/octet-stream")
    @GetMapping({"download"})
    public void download(HttpServletResponse response, String url) throws Exception {
        response.setHeader("Content-Type", "application/octet-stream");

        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(url, "utf-8"));
        ServletOutputStream servletOutputStream = response.getOutputStream();
        downLoadFromUrl(url, (OutputStream) servletOutputStream);
    }


    @ApiOperation("远程文件删除")
    @GetMapping({"deleteById"})
    public Message delete(@ApiParam(value = "文件id", required = true) String id) throws Exception {
        int code = FastDFS.delete(id);
        if (code == 0) {
            this.baseFileService.deleteById(id);
            return MessageUtil.success("删除成功");
        }
        return MessageUtil.error("删除失败");
    }


    @ApiOperation(value = "文件上传", notes = "文件大小限制为3M")
    @PostMapping({"upload"})
    public Message upload(@RequestParam("file") MultipartFile file, HttpServletRequest req) throws Exception {
        String fileName = file.getOriginalFilename();
        String ext_name = fileName.substring(fileName.lastIndexOf(".") + 1);
        if (ext_name.contains("?")) {
            ext_name = ext_name.substring(0, ext_name.indexOf("?"));
        }
        long fileSize = file.getSize();
        if (fileSize > 30145728L) {
            return MessageUtil.error("文件大小不能超过了3M");
        }

        String[] result = FastDFS.upload(file.getBytes(), ext_name);
        if (result != null && result.length > 1) {
            String erpGroupName = result[0];
            String erpFileName = result[1];
            BaseFile baseFile = new BaseFile();
            baseFile.setFileName(fileName);
            baseFile.setId(erpFileName);
            baseFile.setGroupName(erpGroupName);
            baseFile.setUploadTime(Long.valueOf((new Date()).getTime()));
            baseFile.setFileSize(Long.valueOf(fileSize));

            this.baseFileService.save(baseFile);

            return MessageUtil.success("success", baseFile);
        }
        return MessageUtil.error("上传失败");
    }
}


