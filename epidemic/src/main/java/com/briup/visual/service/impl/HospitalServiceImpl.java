package com.briup.visual.service.impl;

import com.briup.visual.bean.Hospital;
import com.briup.visual.dao.HospitalMapper;
import com.briup.visual.dao.extend.HospitalExtendMapper;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IHospitalService;

import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


@Service
public class HospitalServiceImpl
        implements IHospitalService {
    @Resource
    private HospitalMapper hospitalMapper;
    @Resource
    private HospitalExtendMapper hospitalExtendMapper;

    public void saveOrUpdate(Hospital hospital) throws CustomerException {
        if (hospital.getId() != null) {
            this.hospitalMapper.updateByPrimaryKey(hospital);
        } else {
            this.hospitalMapper.insert(hospital);
        }
    }


    public void deleteById(long id) throws CustomerException {
        Hospital hospital = this.hospitalMapper.selectByPrimaryKey(Long.valueOf(id));
        if (hospital == null) {
            throw new CustomerException("要删除的医院信息不存在");
        }
        this.hospitalMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    public PageVM<Hospital> pageQuery(int page, int pageSize, String name) {
        List<Hospital> list = this.hospitalExtendMapper.query(page, pageSize, name);
        long total = this.hospitalExtendMapper.count(name);
        PageVM<Hospital> pagevm = new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
        return pagevm;
    }
}


