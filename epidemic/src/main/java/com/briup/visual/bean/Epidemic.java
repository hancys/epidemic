package com.briup.visual.bean;

import java.io.Serializable;

public class Epidemic implements Serializable {

    private Long id;


    private String country;


    private String province;


    private String city;


    private String area;


    private Long confirmed;


    private Long suspected;


    private Long dead;


    private Long cure;


    private Long severe;


    private Long outside;


    private Long inputTime;


    private static final long serialVersionUID = 1L;


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getCountry() {
        return country;
    }


    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }


    public String getProvince() {
        return province;
    }


    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }


    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }


    public String getArea() {
        return area;
    }


    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }


    public Long getConfirmed() {
        return confirmed;
    }


    public void setConfirmed(Long confirmed) {
        this.confirmed = confirmed;
    }


    public Long getSuspected() {
        return suspected;
    }


    public void setSuspected(Long suspected) {
        this.suspected = suspected;
    }


    public Long getDead() {
        return dead;
    }


    public void setDead(Long dead) {
        this.dead = dead;
    }


    public Long getCure() {
        return cure;
    }


    public void setCure(Long cure) {
        this.cure = cure;
    }


    public Long getSevere() {
        return severe;
    }


    public void setSevere(Long severe) {
        this.severe = severe;
    }


    public Long getOutside() {
        return outside;
    }


    public void setOutside(Long outside) {
        this.outside = outside;
    }


    public Long getInputTime() {
        return inputTime;
    }


    public void setInputTime(Long inputTime) {
        this.inputTime = inputTime;
    }
}