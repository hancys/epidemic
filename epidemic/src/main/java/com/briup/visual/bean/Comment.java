package com.briup.visual.bean;

import java.io.Serializable;

public class Comment implements Serializable {

    private Long id;


    private Long commentTime;


    private Long floor;


    private String status;


    private Long userId;


    private Long articleId;


    private Long parentId;


    private String content;


    private static final long serialVersionUID = 1L;


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public Long getCommentTime() {
        return commentTime;
    }


    public void setCommentTime(Long commentTime) {
        this.commentTime = commentTime;
    }


    public Long getFloor() {
        return floor;
    }


    public void setFloor(Long floor) {
        this.floor = floor;
    }


    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Long getArticleId() {
        return articleId;
    }


    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }


    public Long getParentId() {
        return parentId;
    }


    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}