package com.briup.visual.dao.extend;

import com.briup.visual.bean.BaseRole;
import com.briup.visual.bean.extend.BaseRoleExtend;

import java.util.List;

public interface BaseRoleExtendMapper {
    List<BaseRole> selectByUserId(long id);

    List<BaseRoleExtend> selectAll();
}


