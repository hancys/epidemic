package com.briup.visual.dao;

import com.briup.visual.bean.BaseConfig;
import com.briup.visual.bean.BaseConfigExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BaseConfigMapper {
    long countByExample(BaseConfigExample paramBaseConfigExample);

    int deleteByExample(BaseConfigExample paramBaseConfigExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(BaseConfig paramBaseConfig);

    int insertSelective(BaseConfig paramBaseConfig);

    List<BaseConfig> selectByExample(BaseConfigExample paramBaseConfigExample);

    BaseConfig selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") BaseConfig paramBaseConfig, @Param("example") BaseConfigExample paramBaseConfigExample);

    int updateByExample(@Param("record") BaseConfig paramBaseConfig, @Param("example") BaseConfigExample paramBaseConfigExample);

    int updateByPrimaryKeySelective(BaseConfig paramBaseConfig);

    int updateByPrimaryKey(BaseConfig paramBaseConfig);
}


