package com.briup.visual.web.controller;

import com.briup.visual.bean.BaseUser;
import com.briup.visual.bean.extend.BaseUserExtend;
import com.briup.visual.jacky.JwtTokenUtil;
import com.briup.visual.jacky.Message;
import com.briup.visual.jacky.MessageUtil;
import com.briup.visual.service.IBaseUserService;
import com.briup.visual.vm.UserVM;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(description = "登录相关接口")
@Validated
@RestController
@RequestMapping({"/user"})
public class UserController {
    @Autowired
    private IBaseUserService baseUserService;

    @PostMapping({"login"})
    public Message login(@RequestBody UserVM userVM) {
        BaseUser user = this.baseUserService.login(userVM);

        String token = JwtTokenUtil.createJWT(user.getId(), user.getUsername());

        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        return MessageUtil.success(map);
    }


    @ApiOperation("通过token获取用户的基本信息")
    @GetMapping({"info"})
    public Message info(String token) {
        long id = Long.parseLong(JwtTokenUtil.getUserId(token, "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY="));
        BaseUserExtend baseUserExtend = this.baseUserService.findById(id);
        return MessageUtil.success(baseUserExtend);
    }


    @PostMapping({"logout"})
    public Message logout() {
        return MessageUtil.success("退出成功");
    }
}


