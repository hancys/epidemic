package com.briup.visual.service.impl;

import com.briup.visual.bean.BasePrivilege;
import com.briup.visual.bean.BasePrivilegeExample;
import com.briup.visual.dao.BasePrivilegeMapper;
import com.briup.visual.dao.extend.BasePrivilegeExtendMapper;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.service.IBasePrivilegeService;
import com.briup.visual.vm.PrivilegeTree;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


@Service
public class BasePrivilegeServiceImpl
        implements IBasePrivilegeService {
    @Resource
    private BasePrivilegeMapper basePrivilegeMapper;
    @Resource
    private BasePrivilegeExtendMapper basePrivilegeExtendMapper;

    public List<BasePrivilege> findByUserId(long id) {
        return this.basePrivilegeExtendMapper.selectByUserId(id);
    }


    public List<BasePrivilege> findAll() {
        return this.basePrivilegeMapper.selectByExample(new BasePrivilegeExample());
    }


    public void saveOrUpdate(BasePrivilege privilege) throws CustomerException {
        if (privilege.getId() != null) {
            this.basePrivilegeMapper.updateByPrimaryKey(privilege);
        } else {
            this.basePrivilegeMapper.insert(privilege);
        }
    }


    public List<BasePrivilege> findByParentId(Long parentId) {
        BasePrivilegeExample example = new BasePrivilegeExample();
        if (parentId == null) {
            example.createCriteria().andParentIdIsNull();
        } else {
            example.createCriteria().andParentIdEqualTo(parentId);
        }
        return this.basePrivilegeMapper.selectByExample(example);
    }


    public List<PrivilegeTree> findPrivilegeTree() {
        return this.basePrivilegeExtendMapper.selectAll();
    }


    public List<BasePrivilege> findMenuByUserId(long id) {
        return this.basePrivilegeExtendMapper.selectMenuByUserId(id);
    }


    public void deleteById(long id) throws CustomerException {
        BasePrivilege privilege = this.basePrivilegeMapper.selectByPrimaryKey(Long.valueOf(id));
        if (privilege == null) {
            throw new CustomerException("要删除的权限信息不存在");
        }
        this.basePrivilegeMapper.deleteByPrimaryKey(Long.valueOf(id));
    }
}


