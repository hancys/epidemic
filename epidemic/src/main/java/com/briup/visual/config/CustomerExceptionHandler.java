package com.briup.visual.config;

import com.briup.visual.jacky.*;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class CustomerExceptionHandler {
    @ExceptionHandler({Exception.class})
    public <E> Message handler(Exception exception) {
        if (exception instanceof CustomerException) {
            if (exception instanceof PermissionException) {
                return MessageUtil.forbidden("权限不足");
            }
            if (exception instanceof UnAuthorizedException) {
                return MessageUtil.unAuthorized(exception.getMessage());
            }

            return MessageUtil.error(exception.getMessage());
        }
        if (exception instanceof org.springframework.dao.DataIntegrityViolationException) {
            return MessageUtil.error("该数据暂时不允许删除！请先删除与当前数据关联的其他数据");
        }
        return MessageUtil.error("后台接口异常！");
    }
}


