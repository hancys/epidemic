package com.briup.visual.service;

import com.briup.visual.bean.BasePrivilege;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.vm.PrivilegeTree;

import java.util.List;

public interface IBasePrivilegeService {
    void deleteById(long paramLong) throws CustomerException;

    List<BasePrivilege> findAll();

    List<BasePrivilege> findByParentId(Long paramLong);

    void saveOrUpdate(BasePrivilege paramBasePrivilege) throws CustomerException;

    List<PrivilegeTree> findPrivilegeTree();

    List<BasePrivilege> findByUserId(long paramLong);

    List<BasePrivilege> findMenuByUserId(long paramLong);
}


