package com.briup.visual.service.impl;

import com.briup.visual.bean.Category;
import com.briup.visual.bean.CategoryExample;
import com.briup.visual.dao.CategoryMapper;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.service.ICategoryService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class CategoryServiceImpl
        implements ICategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    public List<Category> findAll() {
        return this.categoryMapper.selectByExample(new CategoryExample());
    }


    public void saveOrUpdate(Category category) throws CustomerException {
        if (category.getId() != null) {
            this.categoryMapper.updateByPrimaryKey(category);
        } else {

            CategoryExample example = new CategoryExample();
            example.createCriteria().andNameEqualTo(category.getName());
            List<Category> list = this.categoryMapper.selectByExample(example);
            if (list.size() > 0) {
                throw new CustomerException("该栏目已经存在");
            }
            this.categoryMapper.insert(category);
        }
    }


    @Transactional
    public void deleteById(long id) throws CustomerException {
        if (id < 8L) {
            throw new CustomerException("内置栏目无法删除");
        }
        Category category = this.categoryMapper.selectByPrimaryKey(Long.valueOf(id));
        if (category == null) {
            throw new CustomerException("要删除的栏目不存在");
        }

        this.categoryMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    @Transactional
    public void batchDelete(long[] ids) throws CustomerException {
        for (long id : ids)
            deleteById(id);
    }
}


