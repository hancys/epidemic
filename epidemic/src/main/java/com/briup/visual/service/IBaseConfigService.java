package com.briup.visual.service;

import com.briup.visual.bean.BaseConfig;

import java.util.List;

public interface IBaseConfigService {
    void saveOrUpdate(BaseConfig paramBaseConfig);

    void deleteById(long paramLong);

    List<BaseConfig> findAll();

    BaseConfig findByKey(String paramString);
}


