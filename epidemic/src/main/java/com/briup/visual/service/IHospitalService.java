package com.briup.visual.service;

import com.briup.visual.bean.Hospital;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;

public interface IHospitalService {
    void saveOrUpdate(Hospital paramHospital) throws CustomerException;

    void deleteById(long paramLong) throws CustomerException;

    PageVM<Hospital> pageQuery(int paramInt1, int paramInt2, String paramString);
}


