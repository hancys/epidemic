package com.briup.visual.jacky;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelUtils {
    public static List<List<Object>> readExcel(File file) throws Exception {
        if (!file.exists()) {
            throw new Exception("找不到文件");
        }
        List<List<Object>> list = new LinkedList<>();
        XSSFWorkbook xwb = new XSSFWorkbook(new FileInputStream(file));

        XSSFSheet sheet = xwb.getSheetAt(0);
        XSSFRow row = null;
        XSSFCell cell = null;
        DecimalFormat df = new DecimalFormat("#");
        for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getPhysicalNumberOfRows() - 1; i++) {
            row = sheet.getRow(i);
            if (row != null) {


                List<Object> linked = new LinkedList();
                for (int j = row.getFirstCellNum(); j <= row.getLastCellNum(); j++) {
                    Object value = null;
                    cell = row.getCell(j);
                    if (cell != null) {


                        switch (cell.getCellType()) {

                            case 1:
                                value = cell.getStringCellValue();
                                break;
                            case 0:
                                value = df.format(cell.getNumericCellValue());
                                break;

                            case 4:
                                value = Boolean.valueOf(cell.getBooleanCellValue());
                                break;

                            case 3:
                                break;
                            default:
                                value = cell.toString();
                                break;
                        }
                        if (value != null && !value.equals("")) {
                            linked.add(value);
                        }
                    }
                }
                if (linked.size() != 0) {
                    list.add(linked);
                }
            }
        }
        xwb.close();
        return list;
    }


    public static List<List<Object>> readExcel(FileInputStream fileInputStream) throws Exception {
        List<List<Object>> list = new LinkedList<>();
        XSSFWorkbook xwb = new XSSFWorkbook(fileInputStream);

        XSSFSheet sheet = xwb.getSheetAt(1);
        XSSFRow row = null;
        XSSFCell cell = null;
        for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getPhysicalNumberOfRows() - 1; i++) {
            row = sheet.getRow(i);
            if (row != null) {


                List<Object> linked = new LinkedList();
                for (int j = row.getFirstCellNum(); j <= row.getLastCellNum(); j++) {
                    Object value = null;
                    cell = row.getCell(j);
                    if (cell != null) {


                        switch (cell.getCellType()) {
                            case 1:
                                value = cell.getStringCellValue();
                                break;
                            case 0:
                                if ("yyyy\"年\"m\"月\"d\"日\";@".equals(cell.getCellStyle().getDataFormatString())) {


                                    value = Double.valueOf(cell.getNumericCellValue());
                                    break;
                                }
                                value = Double.valueOf(cell.getNumericCellValue());
                                break;

                            case 4:
                                value = Boolean.valueOf(cell.getBooleanCellValue());
                                break;
                            case 3:
                                break;
                            default:
                                value = cell.toString();
                                break;
                        }
                        if (value != null && !value.equals("")) {
                            linked.add(value);
                        }
                    }
                }
                if (linked.size() != 0) {
                    list.add(linked);
                }
            }
        }
        xwb.close();
        return list;
    }


    public static void createExcel(HttpServletResponse response, String excelName, String[] headList, String[] fieldList, List<Map<String, Object>> dataList) throws Exception {
        try {
            XSSFWorkbook workbook = new XSSFWorkbook();

            response.setHeader("Content-Type", "application/vnd.ms-excel");

            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(excelName + ".xlsx", "utf-8"));
            ServletOutputStream servletOutputStream = response.getOutputStream();


            try {
                XSSFSheet sheet = workbook.createSheet();

                XSSFRow row = sheet.createRow(0);

                for (int i = 0; i < headList.length; i++) {


                    XSSFCell cell = row.createCell(i);

                    cell.setCellType(1);

                    cell.setCellValue(headList[i]);
                }


                for (int n = 0; n < dataList.size(); n++) {

                    XSSFRow row_value = sheet.createRow(n + 1);
                    Map<String, Object> dataMap = dataList.get(n);

                    for (int j = 0; j < fieldList.length; j++) {


                        XSSFCell cell = row_value.createCell(j);

                        cell.setCellType(1);

                        cell.setCellValue((dataMap.get(fieldList[j]) != null) ? dataMap.get(fieldList[j]).toString() : "");
                    }
                }


                workbook.write((OutputStream) servletOutputStream);
            } finally {

                servletOutputStream.flush();

                servletOutputStream.close();

                workbook.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}


