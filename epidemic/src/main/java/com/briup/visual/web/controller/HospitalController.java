package com.briup.visual.web.controller;

import com.briup.visual.bean.Hospital;
import com.briup.visual.jacky.Message;
import com.briup.visual.jacky.MessageUtil;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IHospitalService;
import com.sun.istack.internal.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(description = "定点医院相关接口")
@Validated
@RestController
@RequestMapping({"/hospital"})
public class HospitalController {
    @Autowired
    private IHospitalService hospitalService;

    @ApiOperation("分页多条件获定点医院信息")
    @GetMapping({"pageQuery"})
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "当前页", required = true, paramType = "query"), @ApiImplicitParam(name = "pageSize", value = "每页大小", required = true, paramType = "query"), @ApiImplicitParam(name = "name", value = "医院名称", paramType = "query")})
    public Message pageQuery(int page, int pageSize, String name) {
        PageVM<Hospital> pageVM = this.hospitalService.pageQuery(page, pageSize, name);
        return MessageUtil.success(pageVM);
    }


    @ApiOperation(value = "保存或更新定点医院信息", notes = "如果参数中包含id后端认为是更新操作，如果参数中不包含id认为是插入操作")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "编号", paramType = "form", required = false), @ApiImplicitParam(name = "name", value = "名称", paramType = "form", required = true), @ApiImplicitParam(name = "introduce", value = "简介", paramType = "form", required = false), @ApiImplicitParam(name = "address", value = "地址", paramType = "form", required = true), @ApiImplicitParam(name = "flags", value = "标志", paramType = "form", required = false), @ApiImplicitParam(name = "province", value = "省份", paramType = "form", required = true), @ApiImplicitParam(name = "city", value = "市", paramType = "form", required = true), @ApiImplicitParam(name = "area", value = "区", paramType = "form", required = true), @ApiImplicitParam(name = "phone", value = "联系方式", paramType = "form", required = false), @ApiImplicitParam(name = "postcode", value = "邮编", paramType = "form", required = false), @ApiImplicitParam(name = "longitude", value = "经度", paramType = "form", required = true), @ApiImplicitParam(name = "latitude", value = "维度", paramType = "form", required = true)})
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(Long id, @NotNull String name, String introduce, @NotNull String address, String flags, @NotNull String province, @NotNull String city, @NotNull String area, String phone, String postcode, @NotNull String longitude, @NotNull String latitude) {
        Hospital hospital = new Hospital();
        hospital.setId(id);
        hospital.setName(name);
        hospital.setIntroduce(introduce);
        hospital.setFlags(flags);
        hospital.setProvince(province);
        hospital.setCity(city);
        hospital.setArea(area);
        hospital.setAddress(address);
        hospital.setLongitude(longitude);
        hospital.setLatitude(latitude);
        hospital.setPhone(phone);
        hospital.setPostcode(postcode);
        this.hospitalService.saveOrUpdate(hospital);
        return MessageUtil.success("更新成功");
    }


    @ApiOperation("删除定点医院信息")
    @GetMapping({"deleteById"})
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "编号", required = true, paramType = "query")})
    public Message deleteById(long id) {
        this.hospitalService.deleteById(id);
        return MessageUtil.success("删除成功");
    }
}


