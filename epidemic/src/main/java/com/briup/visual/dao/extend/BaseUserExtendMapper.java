package com.briup.visual.dao.extend;

import com.briup.visual.bean.BaseUser;
import com.briup.visual.bean.extend.BaseUserExtend;

import java.util.List;

public interface BaseUserExtendMapper {
    BaseUserExtend selectById(long id);

    List<BaseUserExtend> selectAll();

    List<BaseUser> query(int page, int pageSize, String username, String roleName);

    long count(String username, String roleName);

    BaseUser findIDCard(String idCard);

    void insertPeopeleAndStu(BaseUser baseUser);
}


