package com.briup.visual.service;

import com.briup.visual.bean.Epidemic;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;

import java.util.List;

public interface IEpidemicService {
    void saveOrUpdate(Epidemic paramEpidemic) throws CustomerException;

    void deleteById(long paramLong) throws CustomerException;

    PageVM<Epidemic> pageQuery(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, String paramString4);

    List<Epidemic> queryProvinceDataByCountry(String paramString1, String paramString2);
}


