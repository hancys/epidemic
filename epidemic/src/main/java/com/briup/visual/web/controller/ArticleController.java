package com.briup.visual.web.controller;

import com.briup.visual.bean.Article;
import com.briup.visual.bean.extend.ArticleExtend;
import com.briup.visual.jacky.ExcelUtils;
import com.briup.visual.jacky.Message;
import com.briup.visual.jacky.MessageUtil;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IArticleService;
import com.sun.istack.internal.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(description = "新闻资讯相关接口")
@Validated
@RestController
@RequestMapping({"/article"})
public class ArticleController {
    @Autowired
    private IArticleService articleService;

    @ApiOperation("分页多条件获取新闻资讯信息")
    @GetMapping({"pageQuery"})
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "当前页", required = true, paramType = "query"), @ApiImplicitParam(name = "pageSize", value = "每页大小", required = true, paramType = "query"), @ApiImplicitParam(name = "title", value = "新闻资讯标题", paramType = "query"), @ApiImplicitParam(name = "authorId", value = "作者id", paramType = "query"), @ApiImplicitParam(name = "categoryId", value = "分类id", paramType = "query")})
    public Message pageQuery(int page, int pageSize, String title, Long authorId, Long categoryId) {
        PageVM<ArticleExtend> pageVM = this.articleService.pageQuery(page, pageSize, title, authorId, categoryId);
        return MessageUtil.success(pageVM);
    }


    @ApiOperation(value = "将消息导入到Excel中", notes = "注意！测试的时候请将地址粘贴到浏览器地址栏测试", produces = "application/octet-stream")
    @GetMapping({"download"})
    public void download(HttpServletResponse response) throws Exception {
        String excelName = "article_list";
        String[] headList = {"编号", "标题", "内容"};
        String[] fieldList = {"id", "title", "content"};
        List<Map<String, Object>> dataList = new ArrayList<>();
        List<Article> list = this.articleService.findAll();
        for (Article a : list) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", a.getId());
            map.put("title", a.getTitle());
            map.put("content", a.getContent());
            dataList.add(map);
        }

        ExcelUtils.createExcel(response, excelName, headList, fieldList, dataList);
    }

    @ApiOperation("查询所有新闻资讯")
    @GetMapping({"findAll"})
    public Message findAll() {
        List<Article> list = this.articleService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "级联查询新闻资讯", notes = "级联所属分类，作者，评论，项目，图片")
    @GetMapping({"cascadeFindAll"})
    public Message cascadeFindAll() {
        List<ArticleExtend> list = this.articleService.cascadeFindAll();
        return MessageUtil.success(list);
    }


    @ApiOperation("通过id查询新闻资讯")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", paramType = "query")})
    @GetMapping({"findById"})
    public Message findById(long id) {
        ArticleExtend articleExtend = this.articleService.findById(id);
        return MessageUtil.success(articleExtend);
    }


    @ApiOperation("通过id删除")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", paramType = "query", required = true)})
    @GetMapping({"deleteById"})
    public Message deleteById(long id) {
        this.articleService.deleteById(id);
        return MessageUtil.success("删除成功");
    }


    @ApiOperation(value = "保存或更新消息信息", notes = "如果参数中包含id后端认为是更新操作，如果参数中不包含id认为是插入操作")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "编号", paramType = "form", required = false), @ApiImplicitParam(name = "title", value = "标题", paramType = "form", required = true), @ApiImplicitParam(name = "content", value = "内容", paramType = "form", required = true), @ApiImplicitParam(name = "categoryId", value = "所属分类id", paramType = "form", required = true), @ApiImplicitParam(name = "authorId", value = "所属作者id", paramType = "form", required = false), @ApiImplicitParam(name = "photoId", value = "封面图片id", paramType = "form", required = false)})
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(Long id, @NotNull String title, @NotNull String content, @NotNull Long categoryId, Long authorId, Long photoId) {
        Article article = new Article();
        article.setId(id);
        article.setTitle(title);
        article.setContent(content);
        article.setCategoryId(categoryId);
        article.setAuthorId(authorId);
        this.articleService.saveOrUpdate(article);
        return MessageUtil.success("更新成功");
    }


    @ApiOperation("分页获取所有推荐新闻资讯信息")
    @GetMapping({"findAllRecommend"})
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "当前页", required = true, paramType = "query"), @ApiImplicitParam(name = "pageSize", value = "每页大小", required = true, paramType = "query")})
    public Message findAllRecommend(int page, int pageSize) {
        PageVM<ArticleExtend> pageVM = this.articleService.findAllRecommend(page, pageSize);
        return MessageUtil.success(pageVM);
    }
}


