package com.briup.visual.bean;

import java.io.Serializable;

public class BaseFile implements Serializable {

    private String id;


    private String fileName;


    private String groupName;


    private Long uploadTime;


    private Long fileSize;


    private static final long serialVersionUID = 1L;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    public String getFileName() {
        return fileName;
    }


    public void setFileName(String fileName) {
        this.fileName = fileName == null ? null : fileName.trim();
    }


    public String getGroupName() {
        return groupName;
    }


    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }


    public Long getUploadTime() {
        return uploadTime;
    }


    public void setUploadTime(Long uploadTime) {
        this.uploadTime = uploadTime;
    }


    public Long getFileSize() {
        return fileSize;
    }


    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }
}