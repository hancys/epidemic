package com.briup.visual.jacky;

import java.util.List;


public class PageVM<E> {
    private Integer page;
    private Integer pageSize;
    private Long total;
    private List<E> list;

    public PageVM() {
    }

    public PageVM(Integer page, Integer pageSize, Long total, List<E> list) {
        this.page = page;
        this.pageSize = pageSize;
        this.total = total;
        this.list = list;
    }

    public Integer getPage() {
        return this.page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
        return this.total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<E> getList() {
        return this.list;
    }

    public void setList(List<E> list) {
        this.list = list;
    }
}


