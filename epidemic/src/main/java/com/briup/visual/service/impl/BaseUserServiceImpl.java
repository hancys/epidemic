package com.briup.visual.service.impl;

import com.briup.visual.bean.BaseUser;
import com.briup.visual.bean.BaseUserExample;
import com.briup.visual.bean.BaseUserRole;
import com.briup.visual.bean.BaseUserRoleExample;
import com.briup.visual.bean.extend.BaseUserExtend;
import com.briup.visual.dao.BaseUserMapper;
import com.briup.visual.dao.BaseUserRoleMapper;
import com.briup.visual.dao.extend.BaseUserExtendMapper;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IBaseUserService;
import com.briup.visual.vm.UserVM;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


@Service
public class BaseUserServiceImpl
        implements IBaseUserService {
    @Resource
    private BaseUserExtendMapper baseUserExtendMapper;
    @Resource
    private BaseUserMapper baseUserMapper;
    @Resource
    private BaseUserRoleMapper baseUserRoleMapper;

    @Override
    public PageVM<BaseUser> pageQuery(int page, int pageSize, String username, String roleName) {
        List<BaseUser> list = this.baseUserExtendMapper.query(page, pageSize, username, roleName);
        long total = this.baseUserExtendMapper.count(username, roleName);
        PageVM<BaseUser> pageVM = new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
        return pageVM;
    }


    @Override
    public BaseUser login(UserVM userVM) throws CustomerException {
        BaseUserExample example = new BaseUserExample();
        example.createCriteria().andUsernameEqualTo(userVM.getUsername());
        List<BaseUser> list = this.baseUserMapper.selectByExample(example);
        if (list.size() <= 0) {
            throw new CustomerException("该用户不存在");
        }
        BaseUser user = list.get(0);
        if (!user.getPassword().equals(userVM.getPassword())) {
            throw new CustomerException("密码不匹配");
        }
        return user;
    }


    public BaseUserExtend findById(long id) {
        return this.baseUserExtendMapper.selectById(id);
    }


    public List<BaseUser> findAll() {
        return this.baseUserMapper.selectByExample(new BaseUserExample());
    }


    public List<BaseUserExtend> cascadeRoleFindAll() {
        return this.baseUserExtendMapper.selectAll();
    }


    public void saveOrUpdate(BaseUser baseUser) throws CustomerException {
        if (baseUser.getId() != null) {
            this.baseUserMapper.updateByPrimaryKey(baseUser);
        } else {

            BaseUserExample example = new BaseUserExample();
            example.createCriteria().andUsernameEqualTo(baseUser.getUsername());
            List<BaseUser> list = this.baseUserMapper.selectByExample(example);
            if (list.size() > 0) {
                throw new CustomerException("该用户已经被占用");
            }

            baseUser.setRegisterTime(Long.valueOf((new Date()).getTime()));
            baseUser.setStatus("正常");
            this.baseUserMapper.insert(baseUser);
        }
    }


    public void changeStatus(long id, String status) throws CustomerException {
        BaseUserExtend baseUserExtend = findById(id);
        if (baseUserExtend == null) {
            throw new CustomerException("该用户不存在");
        }
        baseUserExtend.setStatus(status);
        this.baseUserMapper.updateByPrimaryKey((BaseUser) baseUserExtend);
    }


    public void deleteById(long id) throws CustomerException {
        BaseUserExtend baseUserExtend = findById(id);
        if (baseUserExtend == null) {
            throw new CustomerException("该用户不存在");
        }
        this.baseUserMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    public void setRoles(long id, List<Long> roles) {
        BaseUserRoleExample example = new BaseUserRoleExample();
        example.createCriteria().andUserIdEqualTo(Long.valueOf(id));

        List<BaseUserRole> list = this.baseUserRoleMapper.selectByExample(example);
        List<Long> oldRoles = new ArrayList<>();
        Iterator<BaseUserRole> iterator = list.iterator();
        while (iterator.hasNext()) {
            oldRoles.add(((BaseUserRole) iterator.next()).getRoleId());
        }


        for (Long roleId : roles) {
            if (!oldRoles.contains(roleId)) {
                BaseUserRole userRole = new BaseUserRole();
                userRole.setRoleId(roleId);
                userRole.setUserId(Long.valueOf(id));
                this.baseUserRoleMapper.insert(userRole);
            }
        }


        for (BaseUserRole userRole : list) {
            if (!roles.contains(userRole.getRoleId())) {
                this.baseUserRoleMapper.deleteByPrimaryKey(userRole.getId());
            }
        }
    }


    public List<BaseUser> findByClazzId(long clazzId) {
        BaseUserExample example = new BaseUserExample();
        return this.baseUserMapper.selectByExample(example);
    }


    public void batchInsert(List<BaseUser> list) throws CustomerException {
        for (BaseUser u : list) {

            u.setRegisterTime(Long.valueOf((new Date()).getTime()));
            u.setStatus("正常");
            u.setUserFace("http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png");
            this.baseUserMapper.insert(u);


            BaseUserRole ur = new BaseUserRole();
            ur.setUserId(u.getId());
            ur.setRoleId(Long.valueOf(6L));
            this.baseUserRoleMapper.insert(ur);
        }
    }
}


