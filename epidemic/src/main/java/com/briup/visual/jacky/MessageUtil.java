package com.briup.visual.jacky;

import java.util.Date;


public class MessageUtil {
    public static Message error(String msg) {
        Message message = new Message();
        message.setStatus(Integer.valueOf(500));
        message.setMessage(msg);
        message.setTimestamp(Long.valueOf((new Date()).getTime()));
        return message;
    }


    public static Message success(String msg) {
        Message message = new Message();
        message.setStatus(Integer.valueOf(200));
        message.setMessage(msg);
        message.setTimestamp(Long.valueOf((new Date()).getTime()));
        return message;
    }


    public static Message success(String msg, Object data) {
        Message message = new Message();
        message.setStatus(Integer.valueOf(200));
        message.setMessage(msg);
        message.setData(data);
        message.setTimestamp(Long.valueOf((new Date()).getTime()));
        return message;
    }


    public static Message success(Object data) {
        Message message = new Message();
        message.setStatus(Integer.valueOf(200));
        message.setMessage("success");
        message.setData(data);
        message.setTimestamp(Long.valueOf((new Date()).getTime()));
        return message;
    }


    public static Message forbidden(String msg) {
        Message message = new Message();
        message.setStatus(Integer.valueOf(403));
        message.setMessage(msg);
        message.setTimestamp(Long.valueOf((new Date()).getTime()));
        return message;
    }


    public static Message unAuthorized(String msg) {
        Message message = new Message();
        message.setStatus(Integer.valueOf(401));
        message.setMessage(msg);
        message.setTimestamp(Long.valueOf((new Date()).getTime()));
        return message;
    }
}


