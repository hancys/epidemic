package com.briup.visual.dao.extend;

import com.briup.visual.bean.BasePrivilege;
import com.briup.visual.vm.PrivilegeTree;

import java.util.List;

public interface BasePrivilegeExtendMapper {
    List<PrivilegeTree> selectAll();

    List<BasePrivilege> selectByParentId(long id);

    List<BasePrivilege> selectByRoleId(long id);

    List<BasePrivilege> selectByUserId(long id);

    List<BasePrivilege> selectMenuByUserId(long id);
}


