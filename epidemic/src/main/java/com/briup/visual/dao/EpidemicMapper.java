package com.briup.visual.dao;

import com.briup.visual.bean.Epidemic;
import com.briup.visual.bean.EpidemicExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface EpidemicMapper {
    long countByExample(EpidemicExample paramEpidemicExample);

    int deleteByExample(EpidemicExample paramEpidemicExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(Epidemic paramEpidemic);

    int insertSelective(Epidemic paramEpidemic);

    List<Epidemic> selectByExample(EpidemicExample paramEpidemicExample);

    Epidemic selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") Epidemic paramEpidemic, @Param("example") EpidemicExample paramEpidemicExample);

    int updateByExample(@Param("record") Epidemic paramEpidemic, @Param("example") EpidemicExample paramEpidemicExample);

    int updateByPrimaryKeySelective(Epidemic paramEpidemic);

    int updateByPrimaryKey(Epidemic paramEpidemic);
}


