package com.briup.visual.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.briup.visual.dao"})
public class MybatisConfig {
}


