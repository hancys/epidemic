package com.briup.visual.service;

import com.briup.visual.bean.BaseRole;
import com.briup.visual.bean.extend.BaseRoleExtend;
import com.briup.visual.jacky.CustomerException;

import java.util.List;

public interface IBaseRoleService {
    void authorization(long paramLong, List<Long> paramList);

    List<BaseRole> findAll();

    List<BaseRoleExtend> cascadePrivilegeFindAll();

    void saveOrUpdate(BaseRole paramBaseRole) throws CustomerException;

    void deleteById(long paramLong) throws CustomerException;
}


