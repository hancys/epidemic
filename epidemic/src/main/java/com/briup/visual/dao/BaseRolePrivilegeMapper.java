package com.briup.visual.dao;

import com.briup.visual.bean.BaseRolePrivilege;
import com.briup.visual.bean.BaseRolePrivilegeExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BaseRolePrivilegeMapper {
    long countByExample(BaseRolePrivilegeExample paramBaseRolePrivilegeExample);

    int deleteByExample(BaseRolePrivilegeExample paramBaseRolePrivilegeExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(BaseRolePrivilege paramBaseRolePrivilege);

    int insertSelective(BaseRolePrivilege paramBaseRolePrivilege);

    List<BaseRolePrivilege> selectByExample(BaseRolePrivilegeExample paramBaseRolePrivilegeExample);

    BaseRolePrivilege selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") BaseRolePrivilege paramBaseRolePrivilege, @Param("example") BaseRolePrivilegeExample paramBaseRolePrivilegeExample);

    int updateByExample(@Param("record") BaseRolePrivilege paramBaseRolePrivilege, @Param("example") BaseRolePrivilegeExample paramBaseRolePrivilegeExample);

    int updateByPrimaryKeySelective(BaseRolePrivilege paramBaseRolePrivilege);

    int updateByPrimaryKey(BaseRolePrivilege paramBaseRolePrivilege);
}


