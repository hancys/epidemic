package com.briup.visual.web.controller;

import com.briup.visual.bean.Epidemic;
import com.briup.visual.jacky.Message;
import com.briup.visual.jacky.MessageUtil;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IEpidemicService;
import com.sun.istack.internal.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(description = "疫情相关接口")
@Validated
@RestController
@RequestMapping({"/epidemic"})
public class EpidemicController {
    @Autowired
    private IEpidemicService epidemicService;

    @ApiOperation("根据id删除")
    @GetMapping({"deleteById"})
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "编号", paramType = "query", required = true)})
    public Message deleteById(long id) {
        this.epidemicService.deleteById(id);
        return MessageUtil.success("删除成功");
    }


    @ApiOperation("获取指定国内所有的省份/州数据")
    @GetMapping({"queryProvinceDataByCountry"})
    @ApiImplicitParams({@ApiImplicitParam(name = "country", value = "国", paramType = "query"), @ApiImplicitParam(name = "inputTime", value = "录入时间，例如2020-09-01", paramType = "query")})
    public Message queryProvinceDataByCountry(String country, String inputTime) {
        List<Epidemic> list = this.epidemicService.queryProvinceDataByCountry(country, inputTime);
        return MessageUtil.success(list);
    }


    @ApiOperation("分页多条件获取疫情信息")
    @GetMapping({"pageQuery"})
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "当前页", required = true, paramType = "query"), @ApiImplicitParam(name = "pageSize", value = "每页大小", required = true, paramType = "query"), @ApiImplicitParam(name = "province", value = "省份", paramType = "query"), @ApiImplicitParam(name = "city", value = "城市", paramType = "query"), @ApiImplicitParam(name = "area", value = "区", paramType = "query"), @ApiImplicitParam(name = "inputTime", value = "录入时间，例如2020-09-01", paramType = "query")})
    public Message pageQuery(int page, int pageSize, String province, String city, String area, String inputTime) {
        PageVM<Epidemic> pageVM = this.epidemicService.pageQuery(page, pageSize, province, city, area, inputTime);
        return MessageUtil.success(pageVM);
    }


    @ApiOperation(value = "保存或更新疫情信息", notes = "如果参数中包含id后端认为是更新操作，如果参数中不包含id认为是插入操作")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "编号", paramType = "form", required = false), @ApiImplicitParam(name = "country", value = "国家", paramType = "form", required = true), @ApiImplicitParam(name = "province", value = "省份", paramType = "form", required = true), @ApiImplicitParam(name = "city", value = "市", paramType = "form", required = true), @ApiImplicitParam(name = "area", value = "区", paramType = "form", required = true), @ApiImplicitParam(name = "confirmed", value = "确诊", paramType = "form", required = true), @ApiImplicitParam(name = "suspected", value = "疑似", paramType = "form", required = true), @ApiImplicitParam(name = "dead", value = "死亡", paramType = "form", required = true), @ApiImplicitParam(name = "cure", value = "治愈", paramType = "form", required = true), @ApiImplicitParam(name = "severe", value = "重症", paramType = "form", required = true), @ApiImplicitParam(name = "outside", value = "境外输入", paramType = "form", required = true)})
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(Long id, @NotNull String country, @NotNull String province, @NotNull String city, @NotNull String area, @NotNull long confirmed, @NotNull long suspected, @NotNull long dead, @NotNull long cure, @NotNull long severe, @NotNull long outside) {
        Epidemic epidemic = new Epidemic();
        epidemic.setId(id);
        epidemic.setCountry(country);
        epidemic.setProvince(province);
        epidemic.setCity(city);
        epidemic.setArea(area);
        epidemic.setConfirmed(Long.valueOf(confirmed));
        epidemic.setSuspected(Long.valueOf(suspected));
        epidemic.setDead(Long.valueOf(dead));
        epidemic.setCure(Long.valueOf(cure));
        epidemic.setSevere(Long.valueOf(severe));
        epidemic.setOutside(Long.valueOf(outside));
        epidemic.setInputTime(Long.valueOf((new Date()).getTime()));
        this.epidemicService.saveOrUpdate(epidemic);
        return MessageUtil.success("更新成功");
    }
}


