package com.briup.visual.dao.extend;

import com.briup.visual.bean.extend.ArticleExtend;

import java.util.List;

public interface ArticleExtendMapper {
    List<ArticleExtend> selectAll();

    ArticleExtend selectById(long id);

    List<ArticleExtend> query(int page, int pageSize, String title, Long authorId, Long categoryId);

    long count(String title, Long authorId, Long categoryId);

    List<ArticleExtend> findAllRecommend(int page, int pageSize);

    long countRecommend();
}


