package com.briup.visual.jacky;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.Key;
import java.util.Date;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JwtTokenUtil {
    private static Logger log = LoggerFactory.getLogger(JwtTokenUtil.class);


    public static final String AUTH_HEADER_KEY = "Authorization";


    public static final String TOKEN_PREFIX = "Bearer ";


    public static final String base64Secret = "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY=";


    private static final String clientId = "098f6bcd4621d373cade4e832627b4f6";

    private static final String name = "restapiuser";

    private static final int expiresSecond = 172800000;


    public static Claims parseJWT(String jsonWebToken, String base64Security) {
        try {
            Claims claims = (Claims) Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(base64Security)).parseClaimsJws(jsonWebToken).getBody();
            return claims;
        } catch (ExpiredJwtException eje) {
            log.error("===== Token过期 =====", (Throwable) eje);
            throw new UnAuthorizedException("token过期");
        } catch (Exception e) {
            log.error("===== token解析异常 =====", e);
            throw new UnAuthorizedException("token解析异常");
        }
    }


    public static String createJWT(Long userId, String username) {
        try {
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

            long nowMillis = System.currentTimeMillis();
            Date now = new Date(nowMillis);


            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY=");
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());


            String encryId = Base64Util.encode(userId.toString());


            JwtBuilder builder = Jwts.builder().setHeaderParam("typ", "JWT").claim("userId", encryId).setSubject(username).setIssuer("098f6bcd4621d373cade4e832627b4f6").setIssuedAt(new Date()).setAudience("restapiuser").signWith(signatureAlgorithm, signingKey);

            int TTLMillis = 172800000;
            if (TTLMillis >= 0) {
                long expMillis = nowMillis + TTLMillis;
                Date exp = new Date(expMillis);
                builder.setExpiration(exp)
                        .setNotBefore(now);
            }


            return builder.compact();
        } catch (Exception e) {
            log.error("签名失败", e);
            throw new UnAuthorizedException("签名失败");
        }
    }


    public static String getUsername(String token, String base64Security) {
        return parseJWT(token, base64Security).getSubject();
    }


    public static String getUserId(String token, String base64Security) {
        String userId = (String) parseJWT(token, base64Security).get("userId", String.class);
        return Base64Util.decode(userId);
    }


    public static boolean isExpiration(String token, String base64Security) {
        return parseJWT(token, base64Security).getExpiration().before(new Date());
    }
}


