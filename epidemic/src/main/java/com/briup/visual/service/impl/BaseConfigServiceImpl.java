package com.briup.visual.service.impl;

import com.briup.visual.bean.BaseConfig;
import com.briup.visual.bean.BaseConfigExample;
import com.briup.visual.dao.BaseConfigMapper;
import com.briup.visual.dao.extend.BaseConfigExtendMapper;
import com.briup.visual.service.IBaseConfigService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BaseConfigServiceImpl
        implements IBaseConfigService {
    @Autowired
    private BaseConfigMapper baseConfigMapper;
    @Autowired
    private BaseConfigExtendMapper baseConfigExtendMapper;

    public void saveOrUpdate(BaseConfig baseConfig) {
        if (baseConfig != null) {
            if (baseConfig.getId() != null) {
                this.baseConfigMapper.updateByPrimaryKey(baseConfig);
            } else {
                this.baseConfigMapper.insert(baseConfig);
            }
        }
    }


    public void deleteById(long id) {
        this.baseConfigMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    public List<BaseConfig> findAll() {
        List<BaseConfig> list = this.baseConfigMapper.selectByExample(new BaseConfigExample());
        return list;
    }


    public BaseConfig findByKey(String name) {
        BaseConfig baseConfig = this.baseConfigExtendMapper.findByKey(name);
        return baseConfig;
    }
}


