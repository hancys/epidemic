package com.briup.visual.service.impl;

import com.briup.visual.bean.BaseRole;
import com.briup.visual.bean.BaseRoleExample;
import com.briup.visual.bean.BaseRolePrivilege;
import com.briup.visual.bean.BaseRolePrivilegeExample;
import com.briup.visual.bean.extend.BaseRoleExtend;
import com.briup.visual.dao.BaseRoleMapper;
import com.briup.visual.dao.BaseRolePrivilegeMapper;
import com.briup.visual.dao.extend.BaseRoleExtendMapper;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.service.IBaseRoleService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


@Service
public class BaseRoleServiceImpl
        implements IBaseRoleService {
    @Resource
    private BaseRoleMapper baseRoleMapper;
    @Resource
    private BaseRoleExtendMapper baseRoleExtendMapper;
    @Resource
    private BaseRolePrivilegeMapper baseRolePrivilegeMapper;

    public void authorization(long roleId, List<Long> privilegeIds) {
        BaseRolePrivilegeExample example = new BaseRolePrivilegeExample();
        example.createCriteria().andRoleIdEqualTo(Long.valueOf(roleId));
        List<BaseRolePrivilege> list = this.baseRolePrivilegeMapper.selectByExample(example);

        List<Long> old_privilegeIds = new ArrayList<>();
        for (BaseRolePrivilege rp : list) {
            old_privilegeIds.add(rp.getPrivilegeId());
        }
        Iterator<Long> iterator;
        for (iterator = privilegeIds.iterator(); iterator.hasNext(); ) {
            long privilegeId = ((Long) iterator.next()).longValue();
            if (!old_privilegeIds.contains(Long.valueOf(privilegeId))) {
                BaseRolePrivilege rp = new BaseRolePrivilege();
                rp.setRoleId(Long.valueOf(roleId));
                rp.setPrivilegeId(Long.valueOf(privilegeId));
                this.baseRolePrivilegeMapper.insert(rp);
            }
        }


        if (old_privilegeIds != null) {
            for (Long privilegeId : old_privilegeIds) {
                if (privilegeId != null && !privilegeIds.contains(privilegeId)) {

                    example.clear();
                    example.createCriteria()
                            .andRoleIdEqualTo(Long.valueOf(roleId))
                            .andPrivilegeIdEqualTo(privilegeId);
                    this.baseRolePrivilegeMapper.deleteByExample(example);
                }
            }
        }
    }


    public List<BaseRole> findAll() {
        return this.baseRoleMapper.selectByExample(new BaseRoleExample());
    }


    public List<BaseRoleExtend> cascadePrivilegeFindAll() {
        return this.baseRoleExtendMapper.selectAll();
    }


    public void saveOrUpdate(BaseRole baseRole) throws CustomerException {
        if (baseRole.getId() != null) {
            this.baseRoleMapper.updateByPrimaryKey(baseRole);
        } else {
            this.baseRoleMapper.insert(baseRole);
        }
    }


    public void deleteById(long id) throws CustomerException {
        BaseRole role = this.baseRoleMapper.selectByPrimaryKey(Long.valueOf(id));
        if (role == null) {
            throw new CustomerException("要删除的角色不存在");
        }
        this.baseRoleMapper.deleteByPrimaryKey(Long.valueOf(id));
    }
}


