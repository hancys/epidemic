package com.briup.visual.service.impl;

import com.briup.visual.bean.Article;
import com.briup.visual.bean.ArticleExample;
import com.briup.visual.bean.extend.ArticleExtend;
import com.briup.visual.dao.ArticleMapper;
import com.briup.visual.dao.extend.ArticleExtendMapper;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IArticleService;

import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


@Service
public class ArticleServiceImpl
        implements IArticleService {
    @Resource
    private ArticleMapper articleMapper;
    @Resource
    private ArticleExtendMapper articleExtendMapper;

    public List<Article> findAll() {
        return this.articleMapper.selectByExample(new ArticleExample());
    }


    public List<ArticleExtend> cascadeFindAll() {
        return this.articleExtendMapper.selectAll();
    }


    public ArticleExtend findById(long id) {
        return this.articleExtendMapper.selectById(id);
    }


    public void saveOrUpdate(Article article) throws CustomerException {
        if (article.getId() != null) {
            Article dbArticle = this.articleMapper.selectByPrimaryKey(article.getId());
            if (dbArticle == null) {
                throw new CustomerException("要修改的数据不存在");
            }
            dbArticle.setTitle(article.getTitle());
            dbArticle.setContent(article.getContent());
            dbArticle.setCategoryId(article.getCategoryId());

            dbArticle.setPublishTime(Long.valueOf((new Date()).getTime()));
            this.articleMapper.updateByPrimaryKey(dbArticle);
        } else {

            ArticleExample example = new ArticleExample();
            example.createCriteria().andTitleEqualTo(article.getTitle());

            List<Article> articles = this.articleMapper.selectByExample(example);
            if (articles.size() > 0) {
                throw new CustomerException("标题不能重复");
            }

            article.setPublishTime(Long.valueOf((new Date()).getTime()));
            article.setStatus("未审核");
            article.setThumpUp(Long.valueOf(0L));
            article.setReadTimes(Long.valueOf(0L));
            this.articleMapper.insert(article);
        }
    }


    public PageVM<ArticleExtend> pageQuery(int page, int pageSize, String title, Long authorId, Long categoryId) {
        List<ArticleExtend> list = this.articleExtendMapper.query(page, pageSize, title, authorId, categoryId);
        long total = this.articleExtendMapper.count(title, authorId, categoryId);
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }


    public void deleteById(long id) throws CustomerException {
        Article article = this.articleMapper.selectByPrimaryKey(Long.valueOf(id));
        if (article == null) {
            throw new CustomerException("要删除的数据不存在");
        }
        this.articleMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    public PageVM<ArticleExtend> findAllRecommend(int page, int pageSize) {
        List<ArticleExtend> list = this.articleExtendMapper.findAllRecommend(page, pageSize);
        long total = this.articleExtendMapper.countRecommend();
        return new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
    }
}


