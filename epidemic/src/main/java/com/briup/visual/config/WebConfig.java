package com.briup.visual.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class WebConfig
        implements WebMvcConfigurer {
    @Bean
    public JwtInterceptor jwtInterceptor() {
        return new JwtInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor((HandlerInterceptor) jwtInterceptor())
                .addPathPatterns(new String[]{"/**"
                }).excludePathPatterns(new String[]{"/index/**", "/swagger-resources/**", "/v2/**", "/swagger-ui.html", "/webjars/**", "/user/login", "/user/logout", "/article/download", "/privilege/findPrivilegeTree"});
    }


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns(new String[]{"*"
                }).allowedMethods(new String[]{"GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"
        }).allowedHeaders(new String[]{"*"
        }).allowCredentials(true)
                .maxAge(3600L);
    }
}


