package com.briup.visual.dao;

import com.briup.visual.bean.Article;
import com.briup.visual.bean.ArticleExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface ArticleMapper {
    long countByExample(ArticleExample paramArticleExample);

    int deleteByExample(ArticleExample paramArticleExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(Article paramArticle);

    int insertSelective(Article paramArticle);

    List<Article> selectByExample(ArticleExample paramArticleExample);

    Article selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") Article paramArticle, @Param("example") ArticleExample paramArticleExample);

    int updateByExample(@Param("record") Article paramArticle, @Param("example") ArticleExample paramArticleExample);

    int updateByPrimaryKeySelective(Article paramArticle);

    int updateByPrimaryKey(Article paramArticle);
}


