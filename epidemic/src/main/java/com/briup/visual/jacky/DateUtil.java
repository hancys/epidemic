package com.briup.visual.jacky;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DateUtil {
    public static List<Long> parse(long startTime, long endTime) {
        List<Long> days = new ArrayList<>();
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(new Date(startTime));
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(new Date(endTime));
        tempEnd.add(5, 1);
        while (tempStart.before(tempEnd)) {
            days.add(Long.valueOf(tempStart.getTime().getTime()));
            tempStart.add(6, 1);
        }
        return days;
    }

    public static long daysBetween(long startTime, long endTime) {
        long difference = (startTime - endTime) / 86400000L;
        return Math.abs(difference);
    }
}


