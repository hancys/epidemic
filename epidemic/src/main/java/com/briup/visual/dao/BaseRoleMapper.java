package com.briup.visual.dao;

import com.briup.visual.bean.BaseRole;
import com.briup.visual.bean.BaseRoleExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BaseRoleMapper {
    long countByExample(BaseRoleExample paramBaseRoleExample);

    int deleteByExample(BaseRoleExample paramBaseRoleExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(BaseRole paramBaseRole);

    int insertSelective(BaseRole paramBaseRole);

    List<BaseRole> selectByExample(BaseRoleExample paramBaseRoleExample);

    BaseRole selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") BaseRole paramBaseRole, @Param("example") BaseRoleExample paramBaseRoleExample);

    int updateByExample(@Param("record") BaseRole paramBaseRole, @Param("example") BaseRoleExample paramBaseRoleExample);

    int updateByPrimaryKeySelective(BaseRole paramBaseRole);

    int updateByPrimaryKey(BaseRole paramBaseRole);
}


