package com.briup.visual.web.controller;

import com.briup.visual.bean.BasePrivilege;
import com.briup.visual.bean.BaseUser;
import com.briup.visual.bean.extend.BaseUserExtend;
import com.briup.visual.jacky.Message;
import com.briup.visual.jacky.MessageUtil;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IBasePrivilegeService;
import com.briup.visual.service.IBaseUserService;
import com.briup.visual.vm.UserRoleVM;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(description = "用户相关接口")
@Validated
@RestController
@RequestMapping({"/baseUser"})
public class BaseUserController {
    @Autowired
    private IBaseUserService baseUserService;
    @Autowired
    private IBasePrivilegeService basePrivilegeService;

    @ApiOperation("根据用户名角色名分页查询用户信息")
    @GetMapping({"pageQuery"})
    @ApiImplicitParams({@ApiImplicitParam(name = "page", value = "当前页", required = true, paramType = "query"), @ApiImplicitParam(name = "pageSize", value = "每页大小", required = true, paramType = "query"), @ApiImplicitParam(name = "username", value = "用户名", paramType = "query"), @ApiImplicitParam(name = "rolename", value = "角色名", paramType = "query")})
    public Message pageQuery(int page, int pageSize, String username, String rolename) {
        PageVM<BaseUser> pageVM = this.baseUserService.pageQuery(page, pageSize, username, rolename);
        return MessageUtil.success(pageVM);
    }


    @ApiOperation("通过ID查询权限信息")
    @GetMapping({"findMenuByUserId"})
    public Message findMenuByUserId(long id) {
        List<BasePrivilege> list = this.basePrivilegeService.findMenuByUserId(id);
        return MessageUtil.success(list);
    }

    @ApiOperation("查询所有")
    @GetMapping({"findAll"})
    public Message findAll() {
        List<BaseUser> list = this.baseUserService.findAll();
        return MessageUtil.success(list);
    }

    @ApiOperation(value = "查询所有", notes = "级联用户角色")
    @GetMapping({"cascadeRoleFindAll"})
    public Message cascadeRoleFindAll() {
        List<BaseUserExtend> list = this.baseUserService.cascadeRoleFindAll();
        return MessageUtil.success(list);
    }

    @ApiOperation("保存或更新")
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(BaseUser baseUser) {
        this.baseUserService.saveOrUpdate(baseUser);
        return MessageUtil.success("更新成功");
    }


    @ApiOperation("通过id删除")
    @GetMapping({"deleteById"})
    public Message deleteById(long id) {
        this.baseUserService.deleteById(id);
        return MessageUtil.success("删除成功");
    }

    @ApiOperation("设置权限")
    @PostMapping({"setRoles"})
    public Message setRoles(UserRoleVM userRoleVM) {
        System.out.println(userRoleVM);
        this.baseUserService.setRoles(userRoleVM.getId().longValue(), userRoleVM.getRoles());
        return MessageUtil.success("设置成功");
    }
}


