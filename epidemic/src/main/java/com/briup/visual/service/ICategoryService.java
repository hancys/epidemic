package com.briup.visual.service;

import com.briup.visual.bean.Category;
import com.briup.visual.jacky.CustomerException;

import java.util.List;

public interface ICategoryService {
    List<Category> findAll();

    void saveOrUpdate(Category paramCategory) throws CustomerException;

    void deleteById(long paramLong) throws CustomerException;

    void batchDelete(long[] paramArrayOflong) throws CustomerException;
}


