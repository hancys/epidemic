package com.briup.visual.dao.extend;

import com.briup.visual.bean.BaseConfig;

public interface BaseConfigExtendMapper {
    BaseConfig findByKey(String name);
}


