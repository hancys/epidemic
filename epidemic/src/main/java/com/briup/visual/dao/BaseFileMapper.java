package com.briup.visual.dao;

import com.briup.visual.bean.BaseFile;
import com.briup.visual.bean.BaseFileExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BaseFileMapper {
    long countByExample(BaseFileExample paramBaseFileExample);

    int deleteByExample(BaseFileExample paramBaseFileExample);

    int deleteByPrimaryKey(String paramString);

    int insert(BaseFile paramBaseFile);

    int insertSelective(BaseFile paramBaseFile);

    List<BaseFile> selectByExample(BaseFileExample paramBaseFileExample);

    BaseFile selectByPrimaryKey(String paramString);

    int updateByExampleSelective(@Param("record") BaseFile paramBaseFile, @Param("example") BaseFileExample paramBaseFileExample);

    int updateByExample(@Param("record") BaseFile paramBaseFile, @Param("example") BaseFileExample paramBaseFileExample);

    int updateByPrimaryKeySelective(BaseFile paramBaseFile);

    int updateByPrimaryKey(BaseFile paramBaseFile);
}


