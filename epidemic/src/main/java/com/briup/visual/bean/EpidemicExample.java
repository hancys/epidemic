package com.briup.visual.bean;

import java.util.ArrayList;
import java.util.List;

public class EpidemicExample {

    protected String orderByClause;


    protected boolean distinct;


    protected List<Criteria> oredCriteria;


    public EpidemicExample() {
        oredCriteria = new ArrayList<>();
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }


    public boolean isDistinct() {
        return distinct;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }


    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }


    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }


    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }


    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCountryIsNull() {
            addCriterion("country is null");
            return (Criteria) this;
        }

        public Criteria andCountryIsNotNull() {
            addCriterion("country is not null");
            return (Criteria) this;
        }

        public Criteria andCountryEqualTo(String value) {
            addCriterion("country =", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotEqualTo(String value) {
            addCriterion("country <>", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryGreaterThan(String value) {
            addCriterion("country >", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryGreaterThanOrEqualTo(String value) {
            addCriterion("country >=", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLessThan(String value) {
            addCriterion("country <", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLessThanOrEqualTo(String value) {
            addCriterion("country <=", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLike(String value) {
            addCriterion("country like", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotLike(String value) {
            addCriterion("country not like", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryIn(List<String> values) {
            addCriterion("country in", values, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotIn(List<String> values) {
            addCriterion("country not in", values, "country");
            return (Criteria) this;
        }

        public Criteria andCountryBetween(String value1, String value2) {
            addCriterion("country between", value1, value2, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotBetween(String value1, String value2) {
            addCriterion("country not between", value1, value2, "country");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(String value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(String value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(String value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(String value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(String value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(String value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLike(String value) {
            addCriterion("area like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotLike(String value) {
            addCriterion("area not like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<String> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<String> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(String value1, String value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(String value1, String value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andConfirmedIsNull() {
            addCriterion("confirmed is null");
            return (Criteria) this;
        }

        public Criteria andConfirmedIsNotNull() {
            addCriterion("confirmed is not null");
            return (Criteria) this;
        }

        public Criteria andConfirmedEqualTo(Long value) {
            addCriterion("confirmed =", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedNotEqualTo(Long value) {
            addCriterion("confirmed <>", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedGreaterThan(Long value) {
            addCriterion("confirmed >", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedGreaterThanOrEqualTo(Long value) {
            addCriterion("confirmed >=", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedLessThan(Long value) {
            addCriterion("confirmed <", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedLessThanOrEqualTo(Long value) {
            addCriterion("confirmed <=", value, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedIn(List<Long> values) {
            addCriterion("confirmed in", values, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedNotIn(List<Long> values) {
            addCriterion("confirmed not in", values, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedBetween(Long value1, Long value2) {
            addCriterion("confirmed between", value1, value2, "confirmed");
            return (Criteria) this;
        }

        public Criteria andConfirmedNotBetween(Long value1, Long value2) {
            addCriterion("confirmed not between", value1, value2, "confirmed");
            return (Criteria) this;
        }

        public Criteria andSuspectedIsNull() {
            addCriterion("suspected is null");
            return (Criteria) this;
        }

        public Criteria andSuspectedIsNotNull() {
            addCriterion("suspected is not null");
            return (Criteria) this;
        }

        public Criteria andSuspectedEqualTo(Long value) {
            addCriterion("suspected =", value, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedNotEqualTo(Long value) {
            addCriterion("suspected <>", value, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedGreaterThan(Long value) {
            addCriterion("suspected >", value, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedGreaterThanOrEqualTo(Long value) {
            addCriterion("suspected >=", value, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedLessThan(Long value) {
            addCriterion("suspected <", value, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedLessThanOrEqualTo(Long value) {
            addCriterion("suspected <=", value, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedIn(List<Long> values) {
            addCriterion("suspected in", values, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedNotIn(List<Long> values) {
            addCriterion("suspected not in", values, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedBetween(Long value1, Long value2) {
            addCriterion("suspected between", value1, value2, "suspected");
            return (Criteria) this;
        }

        public Criteria andSuspectedNotBetween(Long value1, Long value2) {
            addCriterion("suspected not between", value1, value2, "suspected");
            return (Criteria) this;
        }

        public Criteria andDeadIsNull() {
            addCriterion("dead is null");
            return (Criteria) this;
        }

        public Criteria andDeadIsNotNull() {
            addCriterion("dead is not null");
            return (Criteria) this;
        }

        public Criteria andDeadEqualTo(Long value) {
            addCriterion("dead =", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadNotEqualTo(Long value) {
            addCriterion("dead <>", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadGreaterThan(Long value) {
            addCriterion("dead >", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadGreaterThanOrEqualTo(Long value) {
            addCriterion("dead >=", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadLessThan(Long value) {
            addCriterion("dead <", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadLessThanOrEqualTo(Long value) {
            addCriterion("dead <=", value, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadIn(List<Long> values) {
            addCriterion("dead in", values, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadNotIn(List<Long> values) {
            addCriterion("dead not in", values, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadBetween(Long value1, Long value2) {
            addCriterion("dead between", value1, value2, "dead");
            return (Criteria) this;
        }

        public Criteria andDeadNotBetween(Long value1, Long value2) {
            addCriterion("dead not between", value1, value2, "dead");
            return (Criteria) this;
        }

        public Criteria andCureIsNull() {
            addCriterion("cure is null");
            return (Criteria) this;
        }

        public Criteria andCureIsNotNull() {
            addCriterion("cure is not null");
            return (Criteria) this;
        }

        public Criteria andCureEqualTo(Long value) {
            addCriterion("cure =", value, "cure");
            return (Criteria) this;
        }

        public Criteria andCureNotEqualTo(Long value) {
            addCriterion("cure <>", value, "cure");
            return (Criteria) this;
        }

        public Criteria andCureGreaterThan(Long value) {
            addCriterion("cure >", value, "cure");
            return (Criteria) this;
        }

        public Criteria andCureGreaterThanOrEqualTo(Long value) {
            addCriterion("cure >=", value, "cure");
            return (Criteria) this;
        }

        public Criteria andCureLessThan(Long value) {
            addCriterion("cure <", value, "cure");
            return (Criteria) this;
        }

        public Criteria andCureLessThanOrEqualTo(Long value) {
            addCriterion("cure <=", value, "cure");
            return (Criteria) this;
        }

        public Criteria andCureIn(List<Long> values) {
            addCriterion("cure in", values, "cure");
            return (Criteria) this;
        }

        public Criteria andCureNotIn(List<Long> values) {
            addCriterion("cure not in", values, "cure");
            return (Criteria) this;
        }

        public Criteria andCureBetween(Long value1, Long value2) {
            addCriterion("cure between", value1, value2, "cure");
            return (Criteria) this;
        }

        public Criteria andCureNotBetween(Long value1, Long value2) {
            addCriterion("cure not between", value1, value2, "cure");
            return (Criteria) this;
        }

        public Criteria andSevereIsNull() {
            addCriterion("severe is null");
            return (Criteria) this;
        }

        public Criteria andSevereIsNotNull() {
            addCriterion("severe is not null");
            return (Criteria) this;
        }

        public Criteria andSevereEqualTo(Long value) {
            addCriterion("severe =", value, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereNotEqualTo(Long value) {
            addCriterion("severe <>", value, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereGreaterThan(Long value) {
            addCriterion("severe >", value, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereGreaterThanOrEqualTo(Long value) {
            addCriterion("severe >=", value, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereLessThan(Long value) {
            addCriterion("severe <", value, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereLessThanOrEqualTo(Long value) {
            addCriterion("severe <=", value, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereIn(List<Long> values) {
            addCriterion("severe in", values, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereNotIn(List<Long> values) {
            addCriterion("severe not in", values, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereBetween(Long value1, Long value2) {
            addCriterion("severe between", value1, value2, "severe");
            return (Criteria) this;
        }

        public Criteria andSevereNotBetween(Long value1, Long value2) {
            addCriterion("severe not between", value1, value2, "severe");
            return (Criteria) this;
        }

        public Criteria andOutsideIsNull() {
            addCriterion("outside is null");
            return (Criteria) this;
        }

        public Criteria andOutsideIsNotNull() {
            addCriterion("outside is not null");
            return (Criteria) this;
        }

        public Criteria andOutsideEqualTo(Long value) {
            addCriterion("outside =", value, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideNotEqualTo(Long value) {
            addCriterion("outside <>", value, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideGreaterThan(Long value) {
            addCriterion("outside >", value, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideGreaterThanOrEqualTo(Long value) {
            addCriterion("outside >=", value, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideLessThan(Long value) {
            addCriterion("outside <", value, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideLessThanOrEqualTo(Long value) {
            addCriterion("outside <=", value, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideIn(List<Long> values) {
            addCriterion("outside in", values, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideNotIn(List<Long> values) {
            addCriterion("outside not in", values, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideBetween(Long value1, Long value2) {
            addCriterion("outside between", value1, value2, "outside");
            return (Criteria) this;
        }

        public Criteria andOutsideNotBetween(Long value1, Long value2) {
            addCriterion("outside not between", value1, value2, "outside");
            return (Criteria) this;
        }

        public Criteria andInputTimeIsNull() {
            addCriterion("input_time is null");
            return (Criteria) this;
        }

        public Criteria andInputTimeIsNotNull() {
            addCriterion("input_time is not null");
            return (Criteria) this;
        }

        public Criteria andInputTimeEqualTo(Long value) {
            addCriterion("input_time =", value, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeNotEqualTo(Long value) {
            addCriterion("input_time <>", value, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeGreaterThan(Long value) {
            addCriterion("input_time >", value, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("input_time >=", value, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeLessThan(Long value) {
            addCriterion("input_time <", value, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeLessThanOrEqualTo(Long value) {
            addCriterion("input_time <=", value, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeIn(List<Long> values) {
            addCriterion("input_time in", values, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeNotIn(List<Long> values) {
            addCriterion("input_time not in", values, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeBetween(Long value1, Long value2) {
            addCriterion("input_time between", value1, value2, "inputTime");
            return (Criteria) this;
        }

        public Criteria andInputTimeNotBetween(Long value1, Long value2) {
            addCriterion("input_time not between", value1, value2, "inputTime");
            return (Criteria) this;
        }
    }


    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }


    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}