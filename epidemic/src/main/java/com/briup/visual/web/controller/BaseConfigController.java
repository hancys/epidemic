package com.briup.visual.web.controller;

import com.briup.visual.bean.BaseConfig;
import com.briup.visual.jacky.Message;
import com.briup.visual.jacky.MessageUtil;
import com.briup.visual.service.IBaseConfigService;
import com.sun.istack.internal.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(description = "配置模块相关接口")
@Validated
@RestController
@RequestMapping({"/baseConfig"})
public class BaseConfigController {
    @Autowired
    private IBaseConfigService baseConfigService;

    @ApiOperation("查询所有配置信息")
    @GetMapping({"/findAll"})
    public Message findAll() {
        List<BaseConfig> list = this.baseConfigService.findAll();
        return MessageUtil.success(list);
    }


    @ApiOperation("根据name查询配置信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "name", value = "配置信息name值", paramType = "query", required = true)})
    @GetMapping({"/findByKey"})
    public Message findByKey(@NotNull String name) {
        BaseConfig baseConfig = this.baseConfigService.findByKey(name);
        return MessageUtil.success(baseConfig);
    }


    @ApiOperation("通过id删除")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", paramType = "query", required = true)})
    @GetMapping({"deleteById"})
    public Message deleteById(long id) {
        this.baseConfigService.deleteById(id);
        return MessageUtil.success("删除成功");
    }


    @ApiOperation(value = "保存或更新配置信息", notes = "如果参数中包含id后端认为是更新操作，如果参数中不包含id认为是插入操作")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", paramType = "form", required = false), @ApiImplicitParam(name = "name", value = "配置名", paramType = "form", required = true), @ApiImplicitParam(name = "val", value = "配置值", paramType = "form", required = true), @ApiImplicitParam(name = "introduce", value = "配置介绍", paramType = "form", required = false)})
    @PostMapping({"saveOrUpdate"})
    public Message saveOrUpdate(Long id, @NotNull String name, @NotNull String val, String introduce) {
        BaseConfig baseConfig = new BaseConfig();
        baseConfig.setId(id);
        baseConfig.setName(name);
        baseConfig.setVal(val);
        baseConfig.setIntroduce(introduce);
        this.baseConfigService.saveOrUpdate(baseConfig);
        return MessageUtil.success("更新成功");
    }
}


