package com.briup.visual.config;

import com.briup.visual.bean.BasePrivilege;
import com.briup.visual.jacky.JwtTokenUtil;
import com.briup.visual.jacky.PermissionException;
import com.briup.visual.jacky.UnAuthorizedException;
import com.briup.visual.service.IBasePrivilegeService;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class JwtInterceptor
        extends HandlerInterceptorAdapter {
    @Autowired
    private IBasePrivilegeService basePrivilegeService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(200);
            return true;
        }


        String token = request.getHeader("Authorization");
        if (StringUtils.isEmpty(token)) {
            throw new UnAuthorizedException("用户还未登录");
        }

        JwtTokenUtil.parseJWT(token, "MDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjY=");


        return true;
    }


    private boolean auth(long userId, String path) {
        List<BasePrivilege> privileges = this.basePrivilegeService.findByUserId(userId);

        for (BasePrivilege p : privileges) {
            if (p.getRoute().matches(path)) {
                return true;
            }
        }
        throw new PermissionException("权限不足");
    }
}


