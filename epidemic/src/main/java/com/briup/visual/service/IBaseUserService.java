package com.briup.visual.service;

import com.briup.visual.bean.BaseUser;
import com.briup.visual.bean.extend.BaseUserExtend;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.vm.UserVM;

import java.util.List;

public interface IBaseUserService {
    PageVM<BaseUser> pageQuery(int paramInt1, int paramInt2, String paramString1, String paramString2);

    void batchInsert(List<BaseUser> paramList) throws CustomerException;

    List<BaseUser> findByClazzId(long paramLong);

    BaseUser login(UserVM paramUserVM) throws CustomerException;

    BaseUserExtend findById(long paramLong);

    List<BaseUser> findAll();

    List<BaseUserExtend> cascadeRoleFindAll();

    void saveOrUpdate(BaseUser paramBaseUser) throws CustomerException;

    void changeStatus(long paramLong, String paramString) throws CustomerException;

    void deleteById(long paramLong) throws CustomerException;

    void setRoles(long paramLong, List<Long> paramList);
}


