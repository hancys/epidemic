package com.briup.visual.jacky;


import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;


public class FastDFS {
    static {
        try {
            ClientGlobal.init("/home/briup/fdfs_client.conf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static int delete(String remote_filename) throws Exception {
        String group_name = "group1";

        try {
            TrackerClient trackerClient = new TrackerClient();
            TrackerServer trackerServer = trackerClient.getConnection();

            StorageServer storageServer = null;
            StorageClient storageClient = new StorageClient(trackerServer, storageServer);
            int result = storageClient.delete_file(group_name, remote_filename);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }


    public static String[] upload(byte[] local_filename, String ext_name) throws Exception {
        try {
            TrackerClient trackerClient = new TrackerClient();
            TrackerServer trackerServer = trackerClient.getConnection();

            StorageServer storageServer = null;
            StorageClient storageClient = new StorageClient(trackerServer, storageServer);

            String groupName = null;

            StorageServer[] storageServers = trackerClient.getStoreStorages(trackerServer, groupName);
            if (storageServers == null) {
                System.out.println("无存储服务器");
            } else {
                int index = 0;
                for (StorageServer s : storageServers) {
                    System.out.println("第" + ++index + "台存储服务器：" + s.getInetSocketAddress());
                }
            }
            NameValuePair[] meta_list = new NameValuePair[0];
            String[] result = storageClient.upload_file(local_filename, ext_name, meta_list);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}


