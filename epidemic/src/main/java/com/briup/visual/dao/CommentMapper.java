package com.briup.visual.dao;

import com.briup.visual.bean.Comment;
import com.briup.visual.bean.CommentExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CommentMapper {
    long countByExample(CommentExample paramCommentExample);

    int deleteByExample(CommentExample paramCommentExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(Comment paramComment);

    int insertSelective(Comment paramComment);

    List<Comment> selectByExampleWithBLOBs(CommentExample paramCommentExample);

    List<Comment> selectByExample(CommentExample paramCommentExample);

    Comment selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") Comment paramComment, @Param("example") CommentExample paramCommentExample);

    int updateByExampleWithBLOBs(@Param("record") Comment paramComment, @Param("example") CommentExample paramCommentExample);

    int updateByExample(@Param("record") Comment paramComment, @Param("example") CommentExample paramCommentExample);

    int updateByPrimaryKeySelective(Comment paramComment);

    int updateByPrimaryKeyWithBLOBs(Comment paramComment);

    int updateByPrimaryKey(Comment paramComment);
}


