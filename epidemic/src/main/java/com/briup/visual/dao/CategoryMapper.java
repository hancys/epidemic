package com.briup.visual.dao;

import com.briup.visual.bean.Category;
import com.briup.visual.bean.CategoryExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CategoryMapper {
    long countByExample(CategoryExample paramCategoryExample);

    int deleteByExample(CategoryExample paramCategoryExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(Category paramCategory);

    int insertSelective(Category paramCategory);

    List<Category> selectByExample(CategoryExample paramCategoryExample);

    Category selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") Category paramCategory, @Param("example") CategoryExample paramCategoryExample);

    int updateByExample(@Param("record") Category paramCategory, @Param("example") CategoryExample paramCategoryExample);

    int updateByPrimaryKeySelective(Category paramCategory);

    int updateByPrimaryKey(Category paramCategory);
}


