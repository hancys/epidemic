package com.briup.visual.service.impl;

import com.briup.visual.bean.Epidemic;
import com.briup.visual.dao.EpidemicMapper;
import com.briup.visual.dao.extend.EpidemicExtendMapper;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;
import com.briup.visual.service.IEpidemicService;

import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;


@Service
public class EpidemicServiceImpl
        implements IEpidemicService {
    @Resource
    private EpidemicMapper epidemicMapper;
    @Resource
    private EpidemicExtendMapper epidemicExtendMapper;

    public void saveOrUpdate(Epidemic epidemic) throws CustomerException {
        if (epidemic.getId() != null) {
            this.epidemicMapper.updateByPrimaryKey(epidemic);
        } else {
            this.epidemicMapper.insert(epidemic);
        }
    }


    public void deleteById(long id) throws CustomerException {
        Epidemic epidemic = this.epidemicMapper.selectByPrimaryKey(Long.valueOf(id));
        if (epidemic == null) {
            throw new CustomerException("要删除的疫情信息不存在");
        }
        this.epidemicMapper.deleteByPrimaryKey(Long.valueOf(id));
    }


    public PageVM<Epidemic> pageQuery(int page, int pageSize, String province, String city, String area, String inputTime) {
        List<Epidemic> list = this.epidemicExtendMapper.query(page, pageSize, province, city, area, inputTime);
        long total = this.epidemicExtendMapper.count(province, city, area, inputTime);
        PageVM<Epidemic> pagevm = new PageVM(Integer.valueOf(page), Integer.valueOf(pageSize), Long.valueOf(total), list);
        return pagevm;
    }


    public List<Epidemic> queryProvinceDataByCountry(String country, String inputTime) {
        return this.epidemicExtendMapper.queryProvinceDataByCountry(country, inputTime);
    }
}


