package com.briup.visual.service;

import com.briup.visual.bean.Article;
import com.briup.visual.bean.extend.ArticleExtend;
import com.briup.visual.jacky.CustomerException;
import com.briup.visual.jacky.PageVM;

import java.util.List;

public interface IArticleService {
    List<Article> findAll();

    List<ArticleExtend> cascadeFindAll();

    ArticleExtend findById(long paramLong);

    void saveOrUpdate(Article paramArticle) throws CustomerException;

    PageVM<ArticleExtend> pageQuery(int paramInt1, int paramInt2, String paramString, Long paramLong1, Long paramLong2);

    void deleteById(long paramLong) throws CustomerException;

    PageVM<ArticleExtend> findAllRecommend(int paramInt1, int paramInt2);
}


