package com.briup.visual.dao;

import com.briup.visual.bean.BasePrivilege;
import com.briup.visual.bean.BasePrivilegeExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BasePrivilegeMapper {
    long countByExample(BasePrivilegeExample paramBasePrivilegeExample);

    int deleteByExample(BasePrivilegeExample paramBasePrivilegeExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(BasePrivilege paramBasePrivilege);

    int insertSelective(BasePrivilege paramBasePrivilege);

    List<BasePrivilege> selectByExample(BasePrivilegeExample paramBasePrivilegeExample);

    BasePrivilege selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") BasePrivilege paramBasePrivilege, @Param("example") BasePrivilegeExample paramBasePrivilegeExample);

    int updateByExample(@Param("record") BasePrivilege paramBasePrivilege, @Param("example") BasePrivilegeExample paramBasePrivilegeExample);

    int updateByPrimaryKeySelective(BasePrivilege paramBasePrivilege);

    int updateByPrimaryKey(BasePrivilege paramBasePrivilege);
}


