package com.briup.visual.service;

import com.briup.visual.bean.BaseFile;

import java.util.List;

public interface IBaseFileService {
    List<BaseFile> findAll();

    void save(BaseFile paramBaseFile) throws Exception;

    void deleteById(String paramString) throws Exception;
}


