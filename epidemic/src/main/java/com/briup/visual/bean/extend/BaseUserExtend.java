package com.briup.visual.bean.extend;

import com.briup.visual.bean.BaseRole;
import com.briup.visual.bean.BaseUser;

import java.util.List;


public class BaseUserExtend
        extends BaseUser {
    private static final long serialVersionUID = 1L;
    public static final String STATUS_NORMAL = "正常";
    public static final String STATUS_FORBIDDEN = "禁用";
    private List<BaseRole> roles;

    public List<BaseRole> getRoles() {
        return this.roles;
    }

    public void setRoles(List<BaseRole> roles) {
        this.roles = roles;
    }
}


