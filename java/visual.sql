/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : 121.199.29.84:3306
 Source Schema         : visual

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 06/09/2020 22:50:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_config
-- ----------------------------
DROP TABLE IF EXISTS `base_config`;
CREATE TABLE `base_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_un` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_config
-- ----------------------------
BEGIN;
INSERT INTO `base_config` VALUES (1, '1', '1', '1');
INSERT INTO `base_config` VALUES (2, '2', '2', '2');
INSERT INTO `base_config` VALUES (3, '3', '3', '3');
COMMIT;

-- ----------------------------
-- Table structure for base_file
-- ----------------------------
DROP TABLE IF EXISTS `base_file`;
CREATE TABLE `base_file` (
  `id` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `group_name` varchar(256) DEFAULT NULL,
  `upload_time` bigint(255) DEFAULT NULL,
  `file_size` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_file
-- ----------------------------
BEGIN;
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-h66Ac6Z6AAB2gw3yd1Q111.jpg', '58252372.jpg', 'group1', 1597933486140, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-hvmATMlHAAEYdVeZwgs377.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1597933305501, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-i7mAIf6yAAB2gw3yd1Q477.jpg', '58252372.jpg', 'group1', 1597934521728, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-izyAXEsrAAB2gw3yd1Q793.jpg', '58252372.jpg', 'group1', 1597934396632, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-i_CAOPt4AAB2gw3yd1Q913.jpg', '58252372.jpg', 'group1', 1597934576592, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-jAyAMEFwAAB2gw3yd1Q758.jpg', '58252372.jpg', 'group1', 1597934604444, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-JjSAVa1DAAA3t7LcvyQ824.png', '授课计划1.png', 'group1', 1597908532932, 14263);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-k0iABzmTAAEYdVeZwgs931.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1597936456978, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-k76AWDH0AAEYdVeZwgs358.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1597936574119, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-kgGAJwNaAAEYdVeZwgs065.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1597936129830, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-kpKAEIFDAAEYdVeZwgs076.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1597936274761, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-kVqAX5XcAAB2gw3yd1Q058.jpg', '58252372.jpg', 'group1', 1597935962811, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-l9GAXt0LAAXEWQYzwG4569.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597937617700, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-lbiAJ3EPAAXEWQYzwG4433.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597937080145, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-lMeAPE3DAA6WlPHTNCY917.jpg', 'sky.jpg', 'group1', 1597936839083, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8-lmyAPE4PAALhPXMPnyw992.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597937260173, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86o4-AM4sTAALhPXMPnyw857.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597678479597, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86o7OAJSSNAALhPXMPnyw857.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597678515951, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86otaABQX-AAXEWQYzwG4159.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597678294989, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86ouKAMki1AAXEWQYzwG4170.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597678306531, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86pVuABxf3AALhPXMPnyw044.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597678939753, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86pZyAccbvAAXEWQYzwG4229.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597679004621, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86qquAYhYPAAEYdVeZwgs527.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1597680299288, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86r1OAOD52AAL5l8OGicA302.png', '企业微信20200430031631.png', 'group1', 1597681491874, 194967);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86rXiAa80UAAMupwt-r9M755.png', 'vue-element-admin项目目录结构.png', 'group1', 1597681016777, 208551);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86sBeAABYIAAL5l8OGicA249.png', '企业微信20200430031631.png', 'group1', 1597681687641, 194967);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86sDSAKMJUAAB2gw3yd1Q004.jpg', '58252372.jpg', 'group1', 1597681716715, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86VziALGINAALhPXMPnyw394.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597658936221, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV86YsiAcKJtAAXEWQYzwG4123.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597661896960, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV872TWAOIJtAALhPXMPnyw731.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597757749079, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV875N2AIYFSAAXEWQYzwG4810.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597760733753, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV875NGAaNqFAALhPXMPnyw821.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597760721029, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV875O-ASw5GAA6WlPHTNCY045.jpg', 'sky.jpg', 'group1', 1597760751532, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV87f9SAMGq-AAEOwbrdVi0610.png', 'lw-test.png', 'group1', 1597734868242, 69313);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV87LkSAOWeGAA6WlPHTNCY977.jpg', 'sky.jpg', 'group1', 1597713988145, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV87nkmAIyMuAAA3t7LcvyQ902.png', '授课计划.png', 'group1', 1597742665592, 14263);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV894m2AbB7gAAB2gw3yd1Q257.jpg', '58252372.jpg', 'group1', 1597891181982, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV894nWAWaiZAAC8RDY8zJ4401.jpg', '2dd.jpg', 'group1', 1597891189635, 48196);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV894XeASXB-AAB2gw3yd1Q262.jpg', '58252372.jpg', 'group1', 1597890935174, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8qd-uAf4BdAAEzbXtgtjg616.png', '构造函数＋原型.png', 'group1', 1596618731720, 78701);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8r99-AZQ4lAAYZaDB_qvk773.jpg', 'user.jpg', 'group1', 1596717023379, 399720);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8_gDOAXDpdAALhPXMPnyw449.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597997107255, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8_gTSALtb-AAXEWQYzwG4362.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597997364042, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8_QYGAJpTUAAXEWQYzwG4012.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1597981057094, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8_RCuAd1SzAALhPXMPnyw125.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597981739054, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV8_RP2AWNSMAALhPXMPnyw552.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1597981949084, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Ce8iAI_QsAAEYdVeZwgs042.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1598192584921, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Ch7WADwrcAAB2gw3yd1Q345.jpg', '58252372.jpg', 'group1', 1598195637245, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9ChoOAAphcAALhPXMPnyw005.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1598195331715, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9ChZmAf3w4AA6WlPHTNCY658.jpg', 'sky.jpg', 'group1', 1598195097033, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9CjPqAPryFAAXEWQYzwG4941.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1598196986896, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9CjRuAf4qiAALhPXMPnyw379.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1598197019051, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9CjUqAPCJbAALhPXMPnyw188.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1598197066794, 188733);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9CkZaAWCqpAAB2gw3yd1Q003.jpg', '58252372.jpg', 'group1', 1598198166172, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Ck_2AJQRNAAB2gw3yd1Q677.jpg', '58252372.jpg', 'group1', 1598198782014, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9ClHqANQ5aAAB2gw3yd1Q693.jpg', '58252372.jpg', 'group1', 1598198906750, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9ClM2AD8B0AAXEWQYzwG4349.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1598198989853, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9ClomAB-o0AA6WlPHTNCY619.jpg', 'sky.jpg', 'group1', 1598199433376, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9ClXuAJxr1AA6WlPHTNCY518.jpg', 'sky.jpg', 'group1', 1598199163614, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DabeAeQgyAAQ8l7GPI1I663.png', '20200623102831.png', 'group1', 1598253495359, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DajiATFEIAAQ8l7GPI1I883.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.aJWcGlg4GtPm7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598253624308, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Dbc6AGKRuAAQ8l7GPI1I729.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.LCIrqK6EP9KI7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254542540, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DbEuAFOYSAAQ8l7GPI1I573.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.76jSvAGBfhUK7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254155266, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DbgOAX8sjAAQ8l7GPI1I130.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.cosxSKw5INhB7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254595835, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DbjaARiAJAAQ8l7GPI1I456.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.7bBlMyx8aHNM7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254646540, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DblmAJGYsAAQ8l7GPI1I178.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.l7fl8TOTbZQu7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254681619, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DbLqAA30RAAQ8l7GPI1I567.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.rbm331TLCViU7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254266586, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DbVaAYtM0AAQ8l7GPI1I493.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.nndQbvyag4xK7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254422574, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DbX-AGiQ8AAQ8l7GPI1I318.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.ICHBdaQ5e8MW7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598254463811, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DccuAXhtnAAQ8l7GPI1I745.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.yfjhD3MygoN77d88f5168940b96667da03e578a21d2c.png', 'group1', 1598255563576, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DceyAdiqQAAQ8l7GPI1I692.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.Hr22o99v0vWO7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598255596836, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DcpSAXm8HAAQ8l7GPI1I925.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.7toPd0pzJqSc7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598255764213, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9De7eARB3tAAXEWQYzwG4651.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1598258103807, 377945);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DEeCAEh4LAA6WlPHTNCY430.jpg', 'sky.jpg', 'group1', 1598231008581, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DevOALh-DAAB2gw3yd1Q020.jpg', '58252372.jpg', 'group1', 1598257907124, 30339);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Dg1-Af9KsAAEYdVeZwgs763.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1598260063194, 71797);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Dg3CADg_GAAQ8l7GPI1I763.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.sNEi4dIFZC8S7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260080940, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Dg3eAXajiAAQ8l7GPI1I293.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.nmNmpXobc3dy7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260087446, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DgvaADf7MAAQ8l7GPI1I888.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.lQNLuTzlkTcA7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598259958936, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DgwSATzK8AAQ8l7GPI1I309.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.SymK5sOpwO7x7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598259972561, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhFiAe5mzAAQ8l7GPI1I732.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.zKjsGyVYsRjJ7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260312863, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhGCAWiKBAAQ8l7GPI1I507.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.racxTR8aNLXp7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260320253, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhHmAaIFOAAL5l8OGicA531.png', '企业微信20200430031631.png', 'group1', 1598260345613, 194967);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9Dhk6AVUX-AAQ8l7GPI1I861.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.tOxTEQCzMqPm7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260814438, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhlaAQDBbAAAo28b9hXI561.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.1i0RVVidgZKN08645ca6db3c55e9226790226bcf28b6.jpg', 'group1', 1598260822813, 10459);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhMWAHqv8AAMupwt-r9M033.png', 'vue-element-admin项目目录结构.png', 'group1', 1598260421942, 208551);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhNWAcxcVAAD3PkOmAcI147.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.uVscx6hq8XP68db442a78b4dee89ceb060f66fde08b4.jpg', 'group1', 1598260437117, 63294);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhP-ADyQsAAQ8l7GPI1I865.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.8jXcq8eMhl1d7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260479947, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhUKAFIOhAAQ8l7GPI1I871.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.ipEbCo6LTwpe7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260546853, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhUuAILuhAAQ8l7GPI1I091.png', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.0TTwgyBMcZoy7d88f5168940b96667da03e578a21d2c.png', 'group1', 1598260555146, 277655);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhXSAJhCMAAAo28b9hXI880.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.AROfJjy1RWZk08645ca6db3c55e9226790226bcf28b6.jpg', 'group1', 1598260596520, 10459);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DhXyAaPpWAAAo28b9hXI936.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.Uk7DYHTwbBnA08645ca6db3c55e9226790226bcf28b6.jpg', 'group1', 1598260604036, 10459);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DiSKAbTh2AAAo28b9hXI524.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.iNRXWjOu9Ocq08645ca6db3c55e9226790226bcf28b6.jpg', 'group1', 1598261538362, 10459);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DL52AQrx2AA6WlPHTNCY119.jpg', 'sky.jpg', 'group1', 1598238621089, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DM0WAHE_8AA6WlPHTNCY627.jpg', 'sky.jpg', 'group1', 1598239557525, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DM4SAT1UQAA6WlPHTNCY331.jpg', 'sky.jpg', 'group1', 1598239620330, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DMJaAJD-fAA6WlPHTNCY645.jpg', 'sky.jpg', 'group1', 1598238870129, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DWb6ActzSAA6WlPHTNCY692.jpg', 'sky.jpg', 'group1', 1598249406329, 956052);
INSERT INTO `base_file` VALUES ('M00/00/06/rBD-SV9DWruAdtplAAXEWQYzwG4639.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1598249659292, 377945);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D5-SAauXwAAMupwt-r9M442.png', 'vue-element-admin项目目录结构.png', 'group1', 1598285796729, 208551);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D55-AEcOAAALhPXMPnyw002.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1598285727966, 188733);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D56qAeTdHAAXEWQYzwG4785.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1598285738648, 377945);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D58KAKjcSAA6WlPHTNCY522.jpg', 'sky.jpg', 'group1', 1598285762191, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D59SAeLzxAAL5l8OGicA967.png', '企业微信20200430031631.png', 'group1', 1598285780976, 194967);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D5eqADe9VAALhPXMPnyw141.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1598285290839, 188733);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D5gaAf8s2AAXEWQYzwG4859.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1598285318705, 377945);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D5mmALGMcAAB2gw3yd1Q350.jpg', '58252372.jpg', 'group1', 1598285417266, 30339);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D5y-AJ_-sAAB2gw3yd1Q665.jpg', '58252372.jpg', 'group1', 1598285615413, 30339);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D5YuAB6TsAAB2gw3yd1Q417.jpg', '58252372.jpg', 'group1', 1598285195159, 30339);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D5_eAYEl9AAEYdVeZwgs049.png', '1bg6fxni6he08g0my1bic9fjc7n7.png', 'group1', 1598285815508, 71797);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D6UeAU8hxAAA8pHhvDS4485.png', 'jq22-1.png', 'group1', 1598286151813, 15524);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D6VCAVOj2AAAmjjPqJ_Y731.png', 'jq22-2.png', 'group1', 1598286160500, 9870);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D6VmAI8cjAAAk47UyTaU079.png', 'jq22-3.png', 'group1', 1598286169067, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D6WuAIkIGAAAuMH9UV8s312.png', 'jq22-4.png', 'group1', 1598286187371, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D6XmAJpbNAAAk47UyTaU420.png', 'jq22-3.png', 'group1', 1598286201018, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D7ACABG2CAAAuMH9UV8s100.png', 'jq22-4.png', 'group1', 1598286848359, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9D7BqABy1MAA6WlPHTNCY571.jpg', '2.jpg', 'group1', 1598286874278, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9DiSqADpnmAAAo28b9hXI331.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.f8ZYyZAWyvv408645ca6db3c55e9226790226bcf28b6.jpg', 'group1', 1598261546300, 10459);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9DiW6Ae9W6AAAo28b9hXI657.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.Uvbq9C5lMTDj08645ca6db3c55e9226790226bcf28b6.jpg', 'group1', 1598261614867, 10459);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9DiXaAZdQBAAAo28b9hXI812.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.gHg9uzt1b9XK08645ca6db3c55e9226790226bcf28b6.jpg', 'group1', 1598261622759, 10459);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E0P2AFHAgAAA8pHhvDS4763.png', 'jq22-1.png', 'group1', 1598345469464, 15524);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E0PKABST3AAAuMH9UV8s745.png', 'jq22-4.png', 'group1', 1598345458171, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E0QeAV0q8AAAmjjPqJ_Y736.png', 'jq22-2.png', 'group1', 1598345479975, 9870);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E0RCAX4rwAAAk47UyTaU710.png', 'jq22-3.png', 'group1', 1598345488670, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E0SWAdVRAAAAuMH9UV8s181.png', 'jq22-4.png', 'group1', 1598345509780, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E0yuAcFvWAAAuMH9UV8s628.png', 'jq22-4.png', 'group1', 1598346027191, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E1CqAdR_IAAAuMH9UV8s875.png', 'jq22-4.png', 'group1', 1598346282349, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9E1gOAN0CjAAAk47UyTaU141.png', 'jq22-3.png', 'group1', 1598346755556, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9Er92AaCQaAAAk47UyTaU076.png', 'jq22-3.png', 'group1', 1598336989575, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9EstWAJFIZAAAk47UyTaU885.png', 'jq22-3.png', 'group1', 1598337749765, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9EuF2AE5-5AAAmjjPqJ_Y689.png', 'jq22-2.png', 'group1', 1598339165021, 9870);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FOG-AMxDiAAA8pHhvDS4967.png', 'jq22-1.png', 'group1', 1598371951896, 15524);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FOH-AfDygAAAk47UyTaU983.png', 'jq22-3.png', 'group1', 1598371967819, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FOHiAY370AAAmjjPqJ_Y307.png', 'jq22-2.png', 'group1', 1598371960747, 9870);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FOIiAcvfiAAAuMH9UV8s782.png', 'jq22-4.png', 'group1', 1598371976098, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FQKOAe2a-AAEYdVeZwgs002.png', '1.png', 'group1', 1598374051700, 71797);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FQLuAQR-KAA6WlPHTNCY787.jpg', '2.jpg', 'group1', 1598374075274, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FQN6AFlbAAALhPXMPnyw318.jpg', '7f1bdbda-068f-4d81-9a00-d7bf9db02c96.jpg', 'group1', 1598374110497, 188733);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9FQPCASzJtAAXEWQYzwG4277.jpg', '12fd341a-bb13-42ad-a27a-db56c8e5e43c.jpg', 'group1', 1598374128438, 377945);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9GD9SATftkAAAbziDbE4I024.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.ovaomNV422zZ425bca7ed5d166160e7985cc3593b58c.jpg', 'group1', 1598427092055, 7118);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9GDqOAQPaAAAAe7Aa3Wuc166.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.2NTU8VZ2ULyQd4e9a689615e8c7c2f1b5e31eb5d403c.jpg', 'group1', 1598426787582, 7916);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9GDyGAXl5BAAAe7Aa3Wuc653.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.s5bjflaZkdund4e9a689615e8c7c2f1b5e31eb5d403c.jpg', 'group1', 1598426913314, 7916);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9GDZeAHwcXAAD3PkOmAcI036.jpg', 'wxcb48f956d0a778fb.o6zAJs2c1cA7WlUyyBPcN7Ogw60g.Z4RNIwOXtVzw8db442a78b4dee89ceb060f66fde08b4.jpg', 'group1', 1598426519608, 63294);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9Ha2CAcyO5AAAk47UyTaU155.png', 'jq22-3.png', 'group1', 1598516064420, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9Ha5-ADE_LAAAk47UyTaU963.png', 'jq22-3.png', 'group1', 1598516127533, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9Ha9WAW1wvAAAk47UyTaU476.png', 'jq22-3.png', 'group1', 1598516181611, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HaOqAdOTvAAAuMH9UV8s712.png', 'jq22-4.png', 'group1', 1598515434088, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9Haz2AbKZ4AAAk47UyTaU750.png', 'jq22-3.png', 'group1', 1598516029074, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HceGAZRlNAAAk47UyTaU958.png', 'jq22-3.png', 'group1', 1598517729419, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HELCAbybEAAEYdVeZwgs868.png', '1.png', 'group1', 1598492848371, 71797);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HELqAInhYAA6WlPHTNCY437.jpg', '2.jpg', 'group1', 1598492858067, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HEn2AWDNSAA6WlPHTNCY206.jpg', '2.jpg', 'group1', 1598493309405, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HEPmAXE9zAA6WlPHTNCY698.jpg', '2.jpg', 'group1', 1598492921771, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HETuAdjagAAAuMH9UV8s801.png', 'jq22-4.png', 'group1', 1598492987806, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HEU6AM1Z7AAAk47UyTaU443.png', 'jq22-3.png', 'group1', 1598493006047, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HEWeAdWFRAAAk47UyTaU013.png', 'jq22-3.png', 'group1', 1598493031228, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HEYOAcS5QAAAk47UyTaU220.png', 'jq22-3.png', 'group1', 1598493059055, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HfAeAGH5wAAAk47UyTaU992.png', 'jq22-3.png', 'group1', 1598520327070, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HGU-AXOyrAA6WlPHTNCY720.jpg', '2.jpg', 'group1', 1598495055737, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HYhaATTrhAAAk47UyTaU724.png', 'jq22-3.png', 'group1', 1598513686622, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HYiqAK8_WAAEYdVeZwgs139.png', '1.png', 'group1', 1598513706253, 71797);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HYMmAITcyAA6WlPHTNCY008.jpg', '2.jpg', 'group1', 1598513353026, 956052);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HYUeAQPo4AAA8pHhvDS4193.png', 'jq22-1.png', 'group1', 1598513479416, 15524);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HYVuAMTbEAAAuMH9UV8s214.png', 'jq22-4.png', 'group1', 1598513500000, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HYXCAVuxZAAEYdVeZwgs038.png', '1.png', 'group1', 1598513520313, 71797);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HZrWAeKBTAAAuMH9UV8s198.png', 'jq22-4.png', 'group1', 1598514869546, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9HZteAWueQAAAuMH9UV8s677.png', 'jq22-4.png', 'group1', 1598514903126, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IdnmAFJ3NAAAuMH9UV8s220.png', 'jq22-4.png', 'group1', 1598584441337, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IdqCAcPKpAAAuMH9UV8s132.png', 'jq22-4.png', 'group1', 1598584480818, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IdreAQPspAAAuMH9UV8s340.png', 'jq22-4.png', 'group1', 1598584503750, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9Idv2ATPtuAAAuMH9UV8s663.png', 'jq22-4.png', 'group1', 1598584573993, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IenGAQeGWAAAuMH9UV8s173.png', 'jq22-4.png', 'group1', 1598585457652, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9Iew6Ae0XcAAAmjjPqJ_Y987.png', 'jq22-2.png', 'group1', 1598585614515, 9870);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IrjSAaHhwAAAuMH9UV8s583.png', 'jq22-4.png', 'group1', 1598598708619, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IrneAJAG0AAAuMH9UV8s069.png', 'jq22-4.png', 'group1', 1598598775474, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsCqAYwgGAAAuMH9UV8s922.png', 'jq22-4.png', 'group1', 1598599210433, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsDKAdQeVAAAk47UyTaU708.png', 'jq22-3.png', 'group1', 1598599218401, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsDyAReexAAAk47UyTaU633.png', 'jq22-3.png', 'group1', 1598599228388, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsFmAFSIdAAAk47UyTaU498.png', 'jq22-3.png', 'group1', 1598599257108, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsIOACD5eAAEYdVeZwgs278.png', '1.png', 'group1', 1598599299641, 71797);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsOeATTN2AAAuMH9UV8s887.png', 'jq22-4.png', 'group1', 1598599399418, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsSGAUYThAAAk47UyTaU186.png', 'jq22-3.png', 'group1', 1598599457131, 9443);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9IsTeAT0iJAAAuMH9UV8s454.png', 'jq22-4.png', 'group1', 1598599479892, 11824);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9KOEKAFGVPAAA8pHhvDS4836.png', 'jq22-1.png', 'group1', 1598699586405, 15524);
INSERT INTO `base_file` VALUES ('M00/00/07/rBD-SV9KOFqAMMD8AAAuMH9UV8s940.png', 'jq22-4.png', 'group1', 1598699610251, 11824);
COMMIT;

-- ----------------------------
-- Table structure for base_privilege
-- ----------------------------
DROP TABLE IF EXISTS `base_privilege`;
CREATE TABLE `base_privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `route_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `hidden` int(255) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_privilege_privilege` (`parent_id`),
  CONSTRAINT `fk_privilege_privilege` FOREIGN KEY (`parent_id`) REFERENCES `base_privilege` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_privilege
-- ----------------------------
BEGIN;
INSERT INTO `base_privilege` VALUES (74, '研学项目', '', '/studies/project', '', 'menu', 'money', 0, 2, NULL);
INSERT INTO `base_privilege` VALUES (75, '项目管理', '', '/studies/project/List', '', 'menu', '', 0, NULL, 74);
INSERT INTO `base_privilege` VALUES (76, '报名信息', '', '/studies/apply', '', 'menu', 'form', 0, 3, NULL);
INSERT INTO `base_privilege` VALUES (77, '报名信息', NULL, '/studies/apply/List', NULL, 'menu', NULL, 0, NULL, 76);
INSERT INTO `base_privilege` VALUES (78, '咨询信息', '', '/studies/counsel', '', 'menu', 'guide', 0, 1, NULL);
INSERT INTO `base_privilege` VALUES (79, '咨询信息', NULL, '/studies/counsel/List', NULL, 'menu', NULL, 0, NULL, 78);
INSERT INTO `base_privilege` VALUES (80, '模块管理', '', '/studies/modules', '', 'menu', 'list', 0, 5, NULL);
INSERT INTO `base_privilege` VALUES (81, '轮播图', NULL, '/studies/modules/Carousel', NULL, 'menu', NULL, 0, NULL, 80);
INSERT INTO `base_privilege` VALUES (82, '研学项目', NULL, '/studies/modules/Project', NULL, 'menu', NULL, 0, NULL, 80);
INSERT INTO `base_privilege` VALUES (83, '精彩瞬间', NULL, '/studies/modules/Photo', NULL, 'menu', NULL, 0, NULL, 80);
INSERT INTO `base_privilege` VALUES (84, '常见问题', '', '/studies/modules/Question', '', 'menu', '', 0, NULL, 80);
INSERT INTO `base_privilege` VALUES (85, '学生风采', '', '/studies/modules/Student', '', 'menu', '', 0, NULL, 80);
INSERT INTO `base_privilege` VALUES (86, '关于我们', NULL, '/studies/modules/About', NULL, 'menu', NULL, 0, NULL, 80);
INSERT INTO `base_privilege` VALUES (87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_privilege` VALUES (88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_privilege` VALUES (89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_privilege` VALUES (90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_role
-- ----------------------------
BEGIN;
INSERT INTO `base_role` VALUES (1, '教师');
INSERT INTO `base_role` VALUES (2, '管理员');
INSERT INTO `base_role` VALUES (6, '学生');
INSERT INTO `base_role` VALUES (7, '家长');
COMMIT;

-- ----------------------------
-- Table structure for base_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `base_role_privilege`;
CREATE TABLE `base_role_privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `privilege_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_privilege_role` (`role_id`),
  KEY `fk_role_privilege_privilege` (`privilege_id`),
  CONSTRAINT `fk_role_privilege_privilege` FOREIGN KEY (`privilege_id`) REFERENCES `base_privilege` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_role_privilege_role` FOREIGN KEY (`role_id`) REFERENCES `base_role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_role_privilege
-- ----------------------------
BEGIN;
INSERT INTO `base_role_privilege` VALUES (217, 2, 74);
INSERT INTO `base_role_privilege` VALUES (218, 2, 75);
INSERT INTO `base_role_privilege` VALUES (223, 2, 76);
INSERT INTO `base_role_privilege` VALUES (224, 2, 77);
INSERT INTO `base_role_privilege` VALUES (225, 2, 78);
INSERT INTO `base_role_privilege` VALUES (226, 2, 79);
INSERT INTO `base_role_privilege` VALUES (227, 2, 80);
INSERT INTO `base_role_privilege` VALUES (228, 2, 81);
INSERT INTO `base_role_privilege` VALUES (229, 2, 82);
INSERT INTO `base_role_privilege` VALUES (230, 2, 83);
INSERT INTO `base_role_privilege` VALUES (231, 2, 84);
INSERT INTO `base_role_privilege` VALUES (232, 2, 85);
INSERT INTO `base_role_privilege` VALUES (233, 2, 86);
INSERT INTO `base_role_privilege` VALUES (234, 1, 80);
INSERT INTO `base_role_privilege` VALUES (236, 1, 76);
INSERT INTO `base_role_privilege` VALUES (237, 1, 74);
INSERT INTO `base_role_privilege` VALUES (238, 1, 78);
COMMIT;

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `birth` bigint(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `autograph` varchar(255) DEFAULT NULL,
  `is_free` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_face` varchar(255) DEFAULT NULL,
  `id_card` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nation` varchar(255) DEFAULT NULL,
  `register_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `telephone` (`telephone`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_user
-- ----------------------------
BEGIN;
INSERT INTO `base_user` VALUES (1, 'teacher1', '123321', '18812344321', '张老师', 'female', 1572883200000, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (2, 'teacher2', '123321', '18867541234', '胡老师', 'female', 1572883200000, NULL, '时间尽头农村版把对方就看你发的京东方VB年份的脚步 帮忙你的妇科疾病把你的房间表吃饭吧V领将你的放不进宣传部那地方尽快吧辛苦剧场版白癜风几个线程，颁发的', NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (3, 'teacher3', '123321', '18812344325', '刘老师', 'female', 1572883200000, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (5, 'admin', '123321', '110', '刘老师', '女', 1446656461000, '9923@qq.co', 'hhhh', NULL, '正常', 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9GD9SATftkAAAbziDbE4I024.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (6, 'admin1', '123321', '15512348854', '李总', 'male', 1572883200000, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (8, '张明', '123321', '15512348855', '张明', 'male', 1572883200000, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (9, '周盼', '123321', '15512348859', '周盼', 'female', 1572883200000, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (10, '张琛', '123321', '15512348890', '张琛', 'male', 1572883200000, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (11, '黄小雷', '123321', '15512348868', '黄小雷', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (12, '王红艳', '123321', '15512348806', '王红艳', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (13, '刘兰特', '123321', '15512348807', '刘兰特', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (14, '张兰德', '123321', '15512348808', '张兰德', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (15, '张德全', '123321', '15512346678', '张德全', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (16, '李小露', '123321', '15512347765', '李小露', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (17, '李大路', '123321', '15512348888', '李大路', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145568', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (18, '张晓光', '123321', '15512348998', '张晓光', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145598', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (19, '胡美丽', '123321', '15512348000', '胡美丽', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '051221199403145567', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (20, '张晓丽', '123321', '120', '张晓丽', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145501', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (21, '胡大光', '123321', '15512348112', '胡大光', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145502', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (22, '刘涛', '123321', '15512348113', '刘涛', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145503', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (23, '张强', '123321', '15512348114', '张强', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145504', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (24, '张亮', '123321', '15512348115', '张亮', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145505', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (25, '张晓明', '123321', '15512348116', '张晓明', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145506', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (26, '胡晨', '123321', '15512348117', '胡晨', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145507', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (27, '刘发光', '123321', '15512348118', '刘发光', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145508', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (28, '张二妹', '123321', '15512348119', '张二妹', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/20/rBAACV3SWCmADYOQAABJRZlrGTM789.jpg', '51221199403145509', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (29, '张1', '123321', '140', '张1', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145501', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (30, '刘2', '123321', '155123', '刘2', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145502', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (31, '刘3', '123321', '1', '刘3', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145503', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (32, '张4', '123321', '2', '张4', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145504', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (33, '张5', '123321', '3', '张5', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145505', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (34, '张6', '123321', '4', '张6', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145506', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (35, '胡7', '123321', '5', '胡7', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145507', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (36, '刘9', '123321', '6', '刘9', 'male', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145508', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (37, '张10', '123321', '7', '张10', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145509', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (38, '张11', '123321', '8', '张11', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145509', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (39, '张12', '123321', '9', '张12', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145509', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (40, '张13', '123321', '10', '张13', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145509', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (41, '张14', '123321', '11', '张14', 'female', NULL, NULL, NULL, NULL, '正常', 'http://134.175.154.93:8888/group1/M00/00/1F/rBAACV3ORSiAL_PJAAE66PqFd5A920.png', '51221199403145509', '江苏省苏州市工业园区', NULL, NULL);
INSERT INTO `base_user` VALUES (44, 'guolei', '123', '13', '郭磊', 'male', 1597161600000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_user` VALUES (45, '潘广闯', '123', '12', '潘广闯', 'male', 1596470400000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_user` VALUES (47, 'test01', '123', '14', 'test01', 'female', 1597680000000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `base_user` VALUES (59, '可爱的人', '410723', '15', '潘aa', '男', 1596620484141, NULL, NULL, NULL, NULL, 'b.png', '410723', '河南', '中国', NULL);
INSERT INTO `base_user` VALUES (60, '潘潘', '410721', '16', '潘帅', '男', 1596620484141, NULL, NULL, NULL, NULL, 'a.png', '125465410721', '河南', '中国', NULL);
INSERT INTO `base_user` VALUES (61, NULL, '102535', '17', '张三', '女', 1596620484141, NULL, NULL, '忙碌', NULL, 'c.png', '4107261991102535', '河南省新乡市', '中国', NULL);
INSERT INTO `base_user` VALUES (62, '', '102535', '18', '李四', '女', 1596620484141, NULL, NULL, '空闲', '', 'c.png', '41072619911025', '河南省新乡市', '中国', NULL);
INSERT INTO `base_user` VALUES (63, NULL, '726199', '19', '王五', '女', 1596620484141, NULL, NULL, '忙碌', NULL, 'c.png', '410726199', '河南省新乡市', '中国', NULL);
INSERT INTO `base_user` VALUES (69, 'o4iY2XHq7a', '111111', '25', '崔高翔', '男', 1596620484141, NULL, NULL, NULL, NULL, 'a.png', '1111111111111', '山西', '中国', NULL);
INSERT INTO `base_user` VALUES (70, 'P912e4U26H', '222222', '26', '无敌', '男', 1596620484141, NULL, NULL, NULL, NULL, 'b.png', '22222222222222222222', '河南', '中国', NULL);
INSERT INTO `base_user` VALUES (71, 'nQb6hix9t8', 'dfadfv', '1234432123', '123', '女', 1597075200000, NULL, NULL, '空闲', NULL, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9HaOqAdOTvAAAuMH9UV8s712.png', 'wedfadfv', 'adfvadfv', 'adfv', NULL);
INSERT INTO `base_user` VALUES (72, '0Az3kbZ61z', '567876', 'sdv', 'asdvaa', '男', 1598198400000, NULL, NULL, '空闲', NULL, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9Ha9WAW1wvAAAk47UyTaU476.png', '1234567876', 'asdva', 'asdv', NULL);
INSERT INTO `base_user` VALUES (73, '11LdF4d6va', '234567', '123', 'pan', '男', 1597680000000, NULL, NULL, '空闲', NULL, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9HfAeAGH5wAAAk47UyTaU992.png', '1234567', '123456', '123', NULL);
INSERT INTO `base_user` VALUES (83, '44', '123321', '15925632541', 'ff', '女', 1600617600000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, 1598941237621);
INSERT INTO `base_user` VALUES (84, 'lll', '123321', '18979283718', '刘涛', '男', 955296000000, '', '', '', '正常', '', '', '', '', 1598966149478);
INSERT INTO `base_user` VALUES (85, '电话电话', '211', '1e21131121231', '121e', '男', 1600617600000, '', '', '', '正常', '', '', '', '', 1599144912235);
INSERT INTO `base_user` VALUES (86, 'qweq', 'weqq', 'ewqeqeqeq', 'eqwqe', '男', 1600012800000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, 1599144951602);
INSERT INTO `base_user` VALUES (87, 'ewqeqe', 'eqweq', 'ewqewqewqe', 'ewqeqeq', '男', 1599408000000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, 1599144962462);
INSERT INTO `base_user` VALUES (88, 'eqweqe', 'ewqeq', 'fadqdqw', 'eqwewq', '男', 1601222400000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, 1599144974765);
INSERT INTO `base_user` VALUES (89, 'ewqewqe', 'rewwfca', 'fqfqr3twetwgw', 'qweqwwqdqd', '女', 1600617600000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, 1599144992878);
INSERT INTO `base_user` VALUES (90, 'liutao', 'adhisdhiq', 'dwqdqdqwwrq', 'qweqxq', '男', 1600272000000, NULL, NULL, NULL, '正常', NULL, NULL, NULL, NULL, 1599145009276);
COMMIT;

-- ----------------------------
-- Table structure for base_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_user_role`;
CREATE TABLE `base_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_role_user` (`user_id`),
  KEY `fk_user_role_role` (`role_id`),
  CONSTRAINT `fk_user_role_role` FOREIGN KEY (`role_id`) REFERENCES `base_role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_user_role_user` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of base_user_role
-- ----------------------------
BEGIN;
INSERT INTO `base_user_role` VALUES (7, 2, 2);
INSERT INTO `base_user_role` VALUES (11, 5, 2);
INSERT INTO `base_user_role` VALUES (12, 1, 2);
INSERT INTO `base_user_role` VALUES (17, 3, 2);
INSERT INTO `base_user_role` VALUES (18, 6, 2);
INSERT INTO `base_user_role` VALUES (19, 8, 6);
INSERT INTO `base_user_role` VALUES (20, 9, 6);
INSERT INTO `base_user_role` VALUES (22, 10, 6);
INSERT INTO `base_user_role` VALUES (23, 11, 2);
INSERT INTO `base_user_role` VALUES (24, 12, 2);
INSERT INTO `base_user_role` VALUES (25, 13, 2);
INSERT INTO `base_user_role` VALUES (26, 14, 2);
INSERT INTO `base_user_role` VALUES (27, 15, 2);
INSERT INTO `base_user_role` VALUES (28, 17, 2);
INSERT INTO `base_user_role` VALUES (29, 16, 2);
INSERT INTO `base_user_role` VALUES (32, 20, 6);
INSERT INTO `base_user_role` VALUES (33, 21, 6);
INSERT INTO `base_user_role` VALUES (34, 22, 6);
INSERT INTO `base_user_role` VALUES (35, 23, 6);
INSERT INTO `base_user_role` VALUES (36, 24, 6);
INSERT INTO `base_user_role` VALUES (37, 25, 6);
INSERT INTO `base_user_role` VALUES (38, 26, 6);
INSERT INTO `base_user_role` VALUES (39, 27, 6);
INSERT INTO `base_user_role` VALUES (40, 28, 6);
INSERT INTO `base_user_role` VALUES (41, 19, 6);
INSERT INTO `base_user_role` VALUES (42, 18, 6);
INSERT INTO `base_user_role` VALUES (43, 29, 6);
INSERT INTO `base_user_role` VALUES (44, 30, 6);
INSERT INTO `base_user_role` VALUES (45, 31, 6);
INSERT INTO `base_user_role` VALUES (46, 32, 6);
INSERT INTO `base_user_role` VALUES (47, 33, 6);
INSERT INTO `base_user_role` VALUES (48, 34, 6);
INSERT INTO `base_user_role` VALUES (49, 35, 6);
INSERT INTO `base_user_role` VALUES (50, 36, 6);
INSERT INTO `base_user_role` VALUES (51, 37, 6);
INSERT INTO `base_user_role` VALUES (52, 38, 6);
INSERT INTO `base_user_role` VALUES (53, 39, 6);
INSERT INTO `base_user_role` VALUES (54, 40, 6);
INSERT INTO `base_user_role` VALUES (55, 41, 6);
INSERT INTO `base_user_role` VALUES (60, 47, 1);
INSERT INTO `base_user_role` VALUES (61, 63, 1);
INSERT INTO `base_user_role` VALUES (66, 69, 6);
INSERT INTO `base_user_role` VALUES (67, 70, 7);
INSERT INTO `base_user_role` VALUES (68, 71, 1);
INSERT INTO `base_user_role` VALUES (69, 72, 1);
INSERT INTO `base_user_role` VALUES (70, 73, 1);
COMMIT;

-- ----------------------------
-- Table structure for cms_article
-- ----------------------------
DROP TABLE IF EXISTS `cms_article`;
CREATE TABLE `cms_article` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `publish_time` bigint(20) DEFAULT NULL,
  `read_times` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `thump_up` bigint(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_category` (`category_id`),
  KEY `fk_article_user` (`author_id`),
  KEY `photo_id` (`cover`),
  CONSTRAINT `fk_article_category` FOREIGN KEY (`category_id`) REFERENCES `cms_category` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_article_user` FOREIGN KEY (`author_id`) REFERENCES `base_user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cms_article
-- ----------------------------
BEGIN;
INSERT INTO `cms_article` VALUES (1, '校领导调研东山校区疫情防控和企业复工工作', '我们在研学导师的带领下，领取各自的探索任务书。来 ...', 1583449535781, 125, '审核不通过', 10, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV8r99-AZQ4lAAYZaDB_qvk773.jpg', 1, 3);
INSERT INTO `cms_article` VALUES (2, '我校召开研究生教育和学科建设工作网络会议', '每天中午写字时间，我们班开展了一句话新闻活动，将 ...', 1583449535781, 114, '推荐', 23, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV86pZyAccbvAAXEWQYzwG4229.jpg', 2, 3);
INSERT INTO `cms_article` VALUES (11, '学校疫情防控工作领导小组召开第十五次工作会议', '走进中国最为优秀的两所高校之一清华大学，聆听一堂大师课，启发思考热情。在清华...', 1583449535781, 145, '推荐', 21, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV86sBeAABYIAAL5l8OGicA249.png', 3, 3);
INSERT INTO `cms_article` VALUES (31, 'test-charles-03', '\n在航天技术讲师的引导下，开展空间与航天技术课程， ...', 1590149486258, 190, '推荐', 900, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV86sDSAKMJUAAB2gw3yd1Q004.jpg', 5, 3);
INSERT INTO `cms_article` VALUES (32, 'test-charles-04', 'test04', 1590150340427, 150, '推荐', 23, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV87LkSAOWeGAA6WlPHTNCY977.jpg', 2, 3);
INSERT INTO `cms_article` VALUES (33, 'test-charles-05', 'est', 1590150433030, 123, '推荐', 12, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV872TWAOIJtAALhPXMPnyw731.jpg', 2, 4);
INSERT INTO `cms_article` VALUES (34, 'test-charles-06', 'test', 1590150453056, 158, '推荐', 45, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV875NGAaNqFAALhPXMPnyw821.jpg', 1, 3);
INSERT INTO `cms_article` VALUES (36, 'test-charles-07', 'test', 1590150600197, 122, '审核通过', 32, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV875N2AIYFSAAXEWQYzwG4810.jpg', 1, 4);
INSERT INTO `cms_article` VALUES (39, 'test-lw-3', '这个项目很精彩', 1597819245605, 7, '审核不通过', 2, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV875O-ASw5GAA6WlPHTNCY045.jpg', 1, 3);
INSERT INTO `cms_article` VALUES (45, 'test-lw-4', '这个项目很精彩', 1598231717117, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV894XeASXB-AAB2gw3yd1Q262.jpg', 1, 3);
INSERT INTO `cms_article` VALUES (46, 'test-lw-5', '这个项目很精彩', 1598231810585, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV894nWAWaiZAAC8RDY8zJ4401.jpg', 1, 3);
INSERT INTO `cms_article` VALUES (47, 'test-lw-6', '这个项目很精彩', 1598232965775, 0, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV8qd-uAf4BdAAEzbXtgtjg616.png', 1, 3);
INSERT INTO `cms_article` VALUES (48, 'test', '<p>test</p>', 1598249420076, 20, '审核通过', 5, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV9DWb6ActzSAA6WlPHTNCY692.jpg', 5, 3);
INSERT INTO `cms_article` VALUES (49, '小小绘画家', '<div id=\"u1720\" class=\"ax_default box_2\">\n<div id=\"u1720_div\" class=\"\">&nbsp;</div>\n</div>\n<div id=\"u1722\" class=\"ax_default\" data-left=\"0\" data-top=\"0\" data-width=\"1366\" data-height=\"80\">\n<div id=\"u1723\" class=\"ax_default box_2\">\n<div id=\"u1723_div\" class=\"\">&nbsp;</div>\n</div>\n<div id=\"u1724\" class=\"ax_default _三级标题\" style=\"cursor: pointer;\">\n<div id=\"u1724_div\" class=\"\" tabindex=\"0\">&nbsp;</div>\n<div id=\"u1724_text\" class=\"text \">\n<p>研学天下</p>\n</div>\n</div>\n<div id=\"u1725\" class=\"ax_default\" data-left=\"1260\" data-top=\"17\" data-width=\"46\" data-height=\"46\">\n<div id=\"u1726\" class=\"ax_default box_1\">\n<div id=\"u1726_div\" class=\"\">&nbsp;</div>\n</div>\n<div id=\"u1727\" class=\"ax_default image\"><img id=\"u1727_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/首页/u7.svg\" /></div>\n</div>\n<div id=\"u1728\" class=\"ax_default label\" style=\"cursor: pointer;\">\n<div id=\"u1728_div\" class=\"\" tabindex=\"0\">&nbsp;</div>\n<div id=\"u1728_text\" class=\"text \">\n<p>首页</p>\n</div>\n</div>\n<div id=\"u1729\" class=\"ax_default icon\" style=\"cursor: pointer;\"><img id=\"u1729_img\" class=\"img \" tabindex=\"0\" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/首页/u9.svg\" /></div>\n</div>\n<div id=\"u1730\" class=\"ax_default label\">\n<div id=\"u1730_div\" class=\"\">&nbsp;</div>\n<div id=\"u1730_text\" class=\"text \">\n<p>小小绘画家</p>\n</div>\n</div>\n<div id=\"u1731\" class=\"ax_default\" data-left=\"379\" data-top=\"207\" data-width=\"106\" data-height=\"16\">\n<div id=\"u1732\" class=\"ax_default label\">\n<div id=\"u1732_div\" class=\"\">&nbsp;</div>\n<div id=\"u1732_text\" class=\"text \">\n<p>2020年4月30日</p>\n</div>\n</div>\n<div id=\"u1733\" class=\"ax_default icon\"><img id=\"u1733_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1733.svg\" /></div>\n</div>\n<div id=\"u1734\" class=\"ax_default\" data-left=\"323\" data-top=\"207\" data-width=\"41\" data-height=\"16\">\n<div id=\"u1735\" class=\"ax_default icon\"><img id=\"u1735_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1735.svg\" /></div>\n<div id=\"u1736\" class=\"ax_default label\">\n<div id=\"u1736_div\" class=\"\">&nbsp;</div>\n<div id=\"u1736_text\" class=\"text \">\n<p>10</p>\n</div>\n</div>\n</div>\n<div id=\"u1737\" class=\"ax_default icon\"><img id=\"u1737_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1737.svg\" /></div>\n<div id=\"u1738\" class=\"ax_default label\">\n<div id=\"u1738_div\" class=\"\">&nbsp;</div>\n<div id=\"u1738_text\" class=\"text \">\n<p>朱莉</p>\n</div>\n</div>\n<div id=\"u1739\" class=\"ax_default label\">\n<div id=\"u1739_div\" class=\"\">&nbsp;</div>\n<div id=\"u1739_text\" class=\"text \">\n<p>来自于 - EF 海外学年制语言课程</p>\n</div>\n</div>\n<div id=\"u1740\" class=\"ax_default label\">\n<div id=\"u1740_div\" class=\"\">&nbsp;</div>\n<div id=\"u1740_text\" class=\"text \">\n<p>通过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化</p>\n<p>&nbsp;</p>\n<p>艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设</p>\n<p>&nbsp;</p>\n<p>计的思维，将我们的传统艺术更好的传承下去。</p>\n</div>\n</div>', 1598249665305, 40, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV9DWruAdtplAAXEWQYzwG4639.jpg', 5, 3);
INSERT INTO `cms_article` VALUES (50, '小小绘画家-朱莉', '<h2>小小绘画家</h2>\n<h2>&nbsp; &nbsp; </h2>\n<div id=\"u1740_text\" class=\"text \">\n<p>通过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化</p>\n<p>艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设</p>\n<p>计的思维，将我们的传统艺术更好的传承下去。</p>\n<div id=\"u1742_text\" class=\"text \">\n<p>学生通过一周学习，能够知晓我国传统文化&ldquo;剪纸艺术＋上海灯彩&rdquo;的来龙 去脉，并能够感知剪纸、上海灯彩艺术独特的美学价值</p>\n<p>，借此表达丰富的视觉认 知与情感态度，同时能够循序渐进地锻炼手、脑、心的协调能力。在剪纸技巧上，能够剪出圆形纹、</p>\n<p>月牙形纹、锯齿纹、传统云纹、水滴纹等简单纹样，并能够剪出传统双喜字和上海窗花。 在扎灯技巧上，能够学会平接法、十字</p>\n<p>交叉法、勾合法、柱体骨架法等方法，并能够综合运用以上方法扎出传统兔子灯。</p>\n</div>\n</div>', 1598257912388, 3, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV9DevOALh-DAAB2gw3yd1Q020.jpg', 5, 3);
INSERT INTO `cms_article` VALUES (51, '研学天下', '<div id=\"u1740\" class=\"ax_default label\">\n<div id=\"u1740_text\" class=\"text \">\n<p>过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化</p>\n<p>艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设</p>\n<p>计的思维，将我们的传统艺术更好的传承下去。</p>\n</div>\n</div>\n<div id=\"u1741\" class=\"ax_default image\"><img id=\"u1741_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/研学项目/u700.png\" /></div>\n<div id=\"u1742\" class=\"ax_default label\">\n<div id=\"u1742_div\" class=\"\">&nbsp;</div>\n<div id=\"u1742_text\" class=\"text \">\n<p>学生通过一周学习，能够知晓我国传统文化&ldquo;剪纸艺术＋上海灯彩&rdquo;的来龙 去脉，并能够感知剪纸、上海灯彩艺术独特的美学价值</p>\n<p>，借此表达丰富的视觉认 知与情感态度，同时能够循序渐进地锻炼手、脑、心的协调能力。在剪纸技巧上，能够剪出圆形纹、</p>\n<p>&nbsp;</p>\n<p>月牙形纹、锯齿纹、传统云纹、水滴纹等简单纹样，并能够剪出传统双喜字和上海窗花。 在扎灯技巧上，能够学会平接法、十字</p>\n<p>&nbsp;</p>\n<p>交叉法、勾合法、柱体骨架法等方法，并能够综合运用以上方法扎出传统兔子灯。</p>\n</div>\n</div>', 1598258108282, 11, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV9De7eARB3tAAXEWQYzwG4651.jpg', 5, 3);
INSERT INTO `cms_article` VALUES (52, '小小绘画家 - 凯蒂', '<div id=\"u1730\" class=\"ax_default label\">\n<div id=\"u1730_text\" class=\"text \">\n<h1>小小绘画家</h1>\n<h1 style=\"text-align: left;\"><sup>来自于 - EF 海外学年制语言课程</sup></h1>\n</div>\n</div>\n<div id=\"u1731\" class=\"ax_default\" data-left=\"379\" data-top=\"207\" data-width=\"106\" data-height=\"16\">\n<div id=\"u1733\" class=\"ax_default icon\"><img id=\"u1733_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1733.svg\" /> 2020年4月30日 <img id=\"u1735_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1735.svg\" /> 10 <img id=\"u1737_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1737.svg\" /> 朱莉</div>\n</div>\n<div id=\"u1739\" class=\"ax_default label\">\n<div id=\"u1739_div\" class=\"\">&nbsp;</div>\n<div id=\"u1740_text\" class=\"text \">\n<p><sub>通过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设计的思维，将我们的传统艺术更好的传承下去。</sub></p>\n</div>\n</div>\n<div id=\"u1741\" class=\"ax_default image\"><img id=\"u1741_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/研学项目/u700.png\" /></div>\n<div id=\"u1742\" class=\"ax_default label\">\n<div id=\"u1742_div\" class=\"\">&nbsp;</div>\n<div id=\"u1742_text\" class=\"text \">\n<p><sub>学生通过一周学习，能够知晓我国传统文化&ldquo;剪纸艺术＋上海灯彩&rdquo;的来龙 去脉，并能够感知剪纸、上海灯彩艺术独特的美学价值，借此表达丰富的视觉认 知与情感态度，同时能够循序渐进地锻炼手、脑、心的协调能力。在剪纸技巧上，能够剪出圆形纹、月牙形纹、锯齿纹、传统云纹、水滴纹等简单纹样，并能够剪出传统双喜字和上海窗花。 在扎灯技巧上，能够学会平接法、十字交叉法、勾合法、柱体骨架法等方法，并能够综合运用以上方法扎出传统兔子灯。</sub></p>\n</div>\n</div>\n<div id=\"u1743\" class=\"ax_default image\"><img id=\"u1743_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1743.png\" /></div>', 1598260087653, 1, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV9Dg1-Af9KsAAEYdVeZwgs763.png', 5, 3);
INSERT INTO `cms_article` VALUES (53, '这个夏天不简单', '<div id=\"u1740\" class=\"ax_default label\">\n<div id=\"u1740_text\" class=\"text \">\n<p>通过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化</p>\n<p>&nbsp;</p>\n<p>艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设</p>\n<p>&nbsp;</p>\n<p>计的思维，将我们的传统艺术更好的传承下去。</p>\n</div>\n</div>\n<div id=\"u1741\" class=\"ax_default image\"><img id=\"u1741_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/研学项目/u700.png\" /></div>\n<div id=\"u1742\" class=\"ax_default label\">\n<div id=\"u1742_div\" class=\"\">&nbsp;</div>\n<div id=\"u1742_text\" class=\"text \">\n<p>学生通过一周学习，能够知晓我国传统文化&ldquo;剪纸艺术＋上海灯彩&rdquo;的来龙 去脉，并能够感知剪纸、上海灯彩艺术独特的美学价值</p>\n<p>&nbsp;</p>\n<p>，借此表达丰富的视觉认 知与情感态度，同时能够循序渐进地锻炼手、脑、心的协调能力。在剪纸技巧上，能够剪出圆形纹、</p>\n<p>&nbsp;</p>\n<p>月牙形纹、锯齿纹、传统云纹、水滴纹等简单纹样，并能够剪出传统双喜字和上海窗花。 在扎灯技巧上，能够学会平接法、十字</p>\n<p>&nbsp;</p>\n<p>交叉法、勾合法、柱体骨架法等方法，并能够综合运用以上方法扎出传统兔子灯。</p>\n</div>\n</div>', 1598260348773, 1, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/06/rBD-SV9DhHmAaIFOAAL5l8OGicA531.png', 5, 3);
INSERT INTO `cms_article` VALUES (57, '法国夏令营开始报名了', '<div id=\"u827\" class=\"ax_default label\">\n<div id=\"u827_text\" class=\"text \">\n<p>通过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化</p>\n<p>艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设</p>\n<p>计的思维，将我们的传统艺术更好的传承下去。</p>\n</div>\n</div>\n<div id=\"u828\" class=\"ax_default image\"><img id=\"u828_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx/images/文章/u828.png\" /></div>\n<div id=\"u829\" class=\"ax_default label\">\n<div id=\"u829_div\" class=\"\">&nbsp;</div>\n<div id=\"u829_text\" class=\"text \">\n<p>学生通过一周学习，能够知晓我国传统文化&ldquo;剪纸艺术＋上海灯彩&rdquo;的来龙 去脉，并能够感知剪纸、上海灯彩艺术独特的美学价值</p>\n<p>，借此表达丰富的视觉认 知与情感态度，同时能够循序渐进地锻炼手、脑、心的协调能力。在剪纸技巧上，能够剪出圆形纹、</p>\n<p>月牙形纹、锯齿纹、传统云纹、水滴纹等简单纹样，并能够剪出传统双喜字和上海窗花。 在扎灯技巧上，能够学会平接法、十字</p>\n<p>交叉法、勾合法、柱体骨架法等方法，并能够综合运用以上方法扎出传统兔子灯。</p>\n</div>\n</div>', 1598336993894, 0, '推荐', 0, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9Er92AaCQaAAAk47UyTaU076.png', 5, 4);
INSERT INTO `cms_article` VALUES (58, 'test1', '<p>test</p>', 1598517737833, 0, '审核通过', 0, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9HceGAZRlNAAAk47UyTaU958.png', 5, 4);
INSERT INTO `cms_article` VALUES (59, 'test22', '<p>test</p>', 1598585467456, 1, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9IenGAQeGWAAAuMH9UV8s173.png', 8, 3);
INSERT INTO `cms_article` VALUES (60, '英语夏令营', '<div id=\"u1740\" class=\"ax_default label\">\n<div id=\"u1740_text\" class=\"text \">\n<p>通过夏令营的学习，期望可以为孩子们打开一扇中国传统艺术的窗口，同时 为他们开启剪纸、灯彩、扎染、面塑等传统非物质文化</p>\n<p>&nbsp;</p>\n<p>艺术的殿堂，并且能够从 生活中发现传统艺术的点滴之美，最后还能学以致用，将自己所学到的技能用以 美化生活，具备创新设</p>\n<p>&nbsp;</p>\n<p>计的思维，将我们的传统艺术更好的传承下去。</p>\n</div>\n</div>\n<div id=\"u1741\" class=\"ax_default image\"><img id=\"u1741_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/研学项目/u700.png\" /></div>\n<div id=\"u1742\" class=\"ax_default label\">\n<div id=\"u1742_div\" class=\"\">&nbsp;</div>\n<div id=\"u1742_text\" class=\"text \">\n<p>学生通过一周学习，能够知晓我国传统文化&ldquo;剪纸艺术＋上海灯彩&rdquo;的来龙 去脉，并能够感知剪纸、上海灯彩艺术独特的美学价值</p>\n<p>&nbsp;</p>\n<p>，借此表达丰富的视觉认 知与情感态度，同时能够循序渐进地锻炼手、脑、心的协调能力。在剪纸技巧上，能够剪出圆形纹、</p>\n<p>&nbsp;</p>\n<p>月牙形纹、锯齿纹、传统云纹、水滴纹等简单纹样，并能够剪出传统双喜字和上海窗花。 在扎灯技巧上，能够学会平接法、十字</p>\n<p>&nbsp;</p>\n<p>交叉法、勾合法、柱体骨架法等方法，并能够综合运用以上方法扎出传统兔子灯。</p>\n</div>\n</div>\n<div id=\"u1743\" class=\"ax_default image\"><img id=\"u1743_img\" class=\"img \" src=\"http://39.106.16.56/huangyy/yxtx_admin/images/文章/u1743.png\" /></div>\n<div id=\"u1744\" class=\"ax_default\" data-left=\"288\" data-top=\"1356\" data-width=\"753\" data-height=\"82\">\n<div id=\"u1745\" class=\"ax_default box_1\">\n<div id=\"u1745_div\" class=\"\">&nbsp;</div>\n<div id=\"u1745_text\" class=\"text \">\n<p>&nbsp;</p>\n</div>\n</div>\n</div>', 1598585617368, 1, '未审核', 0, 'http://121.199.29.84:8888/group1/M00/00/07/rBD-SV9Iew6Ae0XcAAAmjjPqJ_Y987.png', 8, 3);
INSERT INTO `cms_article` VALUES (64, 'a', '<p>sadsakr7mu,ut,</p>', 1599059624077, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (65, '11', '<p>sadsakr7mu</p>', 1599059616929, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (68, '巴西新冠确诊病例累计超404万例，圣保罗州疫情最严重', '<p><img src=\"https://imagecloud.thepaper.cn/thepaper/image/86/792/818.jpg\" alt=\"\" width=\"600\" /></p>\n<p>海外网9月4日消息，据巴西卫生部当地时间9月3日晚公布的最新数据，该国单日新增新冠肺炎确诊病例43773例，累计确诊4041638例；新增死亡病例834例，累计死亡病例124614例。目前，巴西确诊病例和死亡病例仍居全球第二，仅次于美国。</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>疫情最严重的圣保罗州累计确诊病例超过82.6万例，死亡病例超3万例。该州州长若昂&middot;多利亚2日宣布，该州8月份共有7017人死于新冠肺炎，病亡人数比7月份减少了14.8%，这是疫情暴发以来，该州病亡人数首次出现月度数值下降，重症监护病房(ICU)入住率稳定，均低于54%。数据显示，目前，巴西26个州和一个联邦区中，只有3个州的最近7天死亡人数平均值有所上升，10个州相对稳定，另外13个州和首都所在的联邦区数据均有所下降。（原题为《巴西单日新增确诊病例逾4.3万例 累计逾404万例》）</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 1599179774368, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (69, '联合国秘书长：新冠疫情正改变世界 国际社会应保持警觉', '<p>&nbsp;</p>\n<p><img src=\"https://inews.gtimg.com/newsapp_bt/0/11438603020/1000\" alt=\"https://inews.gtimg.com/newsapp_bt/0/11438603020/1000\" /></p>\n<p class=\"one-p\">新华社联合国9月2日电（记者王建刚）联合国秘书长古特雷斯2日在&ldquo;亚喀巴进程&rdquo;在线会议上表示，新冠疫情改变了世界，正在成为国际和平与安全的&ldquo;游戏规则改变者&rdquo;，世界正进入一个动荡和不稳定的新阶段。</p>\n<p class=\"one-p\">亚喀巴位于约旦南端，是该国唯一的沿海城市。&ldquo;亚喀巴进程&rdquo;倡议由约旦国王阿卜杜拉二世于2015年发起，旨在加强全球安全和军事协调与合作，在各个区域和利益攸关方之间交流信息，以全面打击恐怖主义。今年会议聚焦新冠疫情及其带来的安全挑战。</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 1599179819796, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (70, '加拿大新冠疫情仍在继续 各省纷纷开学引担忧', '<p><img id=\"0\" style=\"max-width: 640px;\" src=\"http://n.sinaimg.cn/spider202093/134/w649h285/20200903/309e-iypetiw0664581.jpg\" /></p>\n<p>&nbsp;</p>\n<p>　<span>央视网消息：</span>从当地时间9月1日开始，加拿大各地的学校都按照政府的要求开学，但是在每日都有数百例新增新冠病毒感染病例的情况下，开学的决定让人们非常担心，安大略省的教师工会甚至因此把省政府告上了劳资关系委员会。</p>\n<p>　　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 安大略省的四个主要教师工会当地时间8月31日把省政府告上劳资关系委员会，指责省政府要求学校开学的决定违反了工作场所安全法规，置19万教师的健康安全于不顾。对此，安大略省省长福特公开表示，政府会听医学人士的意见，而不会接受教师工会的好战态度。</p>\n<p>&nbsp;</p>', 1599179889286, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (71, '欧洲新冠肺炎疫情反弹 防控形势严峻', '<p data-spm-anchor-id=\"C73544894212.P59792594134.0.i4\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong data-spm-anchor-id=\"C73544894212.P59792594134.0.i7\">央视网消息：</strong>当地时间2日，欧洲多国新冠肺炎疫情反弹严重，疫情防控形势严峻。<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 欧洲疾控中心主任阿蒙2日在布鲁塞尔表示，过去几周，欧洲不少国家新冠肺炎疫情持续加剧，整体疫情形势不容乐观。本周的数据显示，欧洲每10万人中已有46人被感染，与今年3月时候的感染水平相近。而在今年3月之后，这一数据最低曾降至每10万人中有15人被感染。</p>\n<p class=\"photo_img_20190808\" style=\"text-align: center;\"><img src=\"http://p3.img.cctvpic.com/photoworkspace/contentimg/2020/09/03/2020090316344192309.jpg\" alt=\"\" width=\"500\" data-spm-anchor-id=\"C73544894212.P59792594134.0.i3\" /></p>\n<p data-spm-anchor-id=\"C73544894212.P59792594134.0.i6\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 根据法国公共卫生部门9月2日公布的数据显示，截至当天，法国24小时新增新冠肺炎确诊病例7017例，新增聚集性感染39起。据报道，法国新学年开始仅一天，法国各地就有多所学校被曝出现感染病例而被迫关闭。<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 在西班牙，根据西班牙卫生部当天公布的最新数据显示，过去7天内西班牙新增新冠肺炎确诊病例数达到47423例。西班牙卡洛斯三世研究所的研究结果显示，西班牙的第二波新冠肺炎疫情大流行始于7月27日，迄今为止第二波新冠肺炎疫情大流行造成的死亡人数至少有2540人。<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 截至当地时间9月2日，英国单日新增新冠肺炎确诊病例1508例。英国卫生大臣汉考克警告说，欧洲大陆已经出现了第二波因为新冠疫情而入院的人数激增的情况，英国必须竭尽所能阻止这一情况的发生。</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 1599179939109, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (72, '乐动体育迎来复工后特殊的开学季，体育培训招生逆境突围谋转型', '<p><img src=\"http://www.cnr.cn/rdzx/cxxhl/zxxx/20200903/W020200903583893264661.png\" alt=\"图片5.png\" /></p>', 1599180164843, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (73, '美国爆发新一轮抗议游行，示威者喊出新口号：先复工再让学生开学', '<div class=\"article-content\">\n<p><span class=\"bjh-p\">由于肺炎疫情再三在美国遭到拖延，如今已经全面失控。美国政府对于复工问题也不得不得不延迟，甚至联邦政府发言人还指责别的州复工早。甚至美国总统特朗普都想借此拖延选举，为自己赚取更大的获利空间。但是对于美国民众而言，开学过早确实不可接受的，因此，在美国纽约又爆发了反对开学的游行示威活动，老百姓甚至抬着棺材上街，请愿噗有一种老百姓哭诉喊冤的感觉。</span></p>\n<div class=\"img-container\"><img class=\"large\" src=\"https://pics2.baidu.com/feed/810a19d8bc3eb135867c0d31f2e6bed4ff1f4484.png?token=432acba05b48a9ceafbcc854db47240d\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<p><span class=\"bjh-p\">由此可见，对于肺炎的问题，美国百姓还是普遍重视的，但是关于提前开学这一议题的抗议，也体现出美国老百姓对于美国政府新冠肺炎疫情抗击能力方面表示了严重的不信任。在不久前美国一些学校的期末考试中，就有一考场的同学被检测新冠肺炎感染，鉴于其强大的传染能力，导致了一定程度的恐慌。</span></p>\n<div class=\"img-container\"><img class=\"large\" src=\"https://pics2.baidu.com/feed/c83d70cf3bc79f3d79b006f7e959db16708b29a7.png?token=76b438ed03e2be450842b54a9f877a29\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<p><span class=\"bjh-p\">但是对于美国人来说，新冠肺炎已经导致了过长时间的停工停产，人们的生活水平难以得到保障，甚至已经出现了一些居住在南方的美国人逃荒逃到墨西哥的地步。相较于3%的肺炎死亡率，美国老百姓更在意的是，如果不复工，自己就会100%被饿死。</span></p>\n<div class=\"img-container\"><img class=\"large\" src=\"https://pics4.baidu.com/feed/14ce36d3d539b600d9383787bca8232dc75cb70f.png?token=646659ac36a2cb3601bfe7b85a58fac5\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<p><span class=\"bjh-p\">但是如果涉及儿童开学的问题，美国民众就要温和的多，毕竟学习并不是一生中必须要干的事情，但是吃饭总是要的。目前美国依然没有对肺炎疫情作出更好的防控工作。疫情在美国已经呈现了全面失控的态势，全美上下已经近500万人感染，死亡16万，这是除了第二次世界大战和美国内战之外，造成的最惨重的人员伤亡事件，创下了历史记录。</span></p>\n<div class=\"img-container\"><img class=\"large\" src=\"https://pics5.baidu.com/feed/503d269759ee3d6d79212d5010ee7b254d4ade41.png?token=ee9677eeff0e34e65f491456519f7cb3\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<p><span class=\"bjh-p\">现在美国众议院疫情应对小组已经在8月6日开始对学校复课问题举行了听证会，由此可见，关于学校复课，也确实引起了美方高层人员的注意。同时也表明，在美国对于疫情防控工作，老百姓除了自我隔离躲避病毒，完没有任何办法，因为疫情已经完全到了失控的地步。</span></p>\n<div class=\"img-container\"><img class=\"large\" src=\"https://pics7.baidu.com/feed/9825bc315c6034a88f42d74890eb5f530b237652.png?token=76148f1e60210d2dd9d92d7f1f20bc46\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<p><span class=\"bjh-p\">能够一劳永逸解决疫情的方法只有靠疫苗。除此之外，由于美国国内一些宗教势力和反智主义盛行，关于自我隔离这一病毒防治理念，在很多人看来，都是违背上帝亵渎神灵的行为，甚至认为戴口罩的人就要去下地狱，如此恶毒脑残的言论，也注定美国的新冠肺炎疫情防治工作必然困难重重，加之美国大选即将到来。新冠肺炎疫情还会给美国社会带来更多的变化。</span></p>\n</div>', 1599180244514, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (74, '为开学复工做准备', '<p>开学后，假如在校园内有学生出现发热及身体不适等情况，该如何处理？</p>\n<div class=\"contheight\">&nbsp;</div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/340.gif\" alt=\"\" width=\"168\" height=\"177\" /></div>\n<p>学生返校后，学校会做哪些具体防疫措施？</p>\n<div class=\"contheight\">&nbsp;</div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/390.gif\" alt=\"\" width=\"168\" height=\"177\" /></div>\n<p>学校是否设置了专门的隔离室？</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>针对这一系列可能出现的情况，3月5日，区教育体育局在康巴什区第四小学开展了开学疫情防控卫生应急演练。区委常委、副区长柴继亮，原鄂尔多斯市疾控中心副主任、疫情防控专家蔺权德，进行现场观摩指导，康巴什区各中小学校长参加。</p>\n<div class=\"contheight\">&nbsp;</div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/446.jpg\" alt=\"\" width=\"1080\" height=\"720\" /></div>\n<p>按照&ldquo;演练即是实战&rdquo;的要求，学校严格根据预案执行校园内外全方位消毒，学生入校体温监测等环节展开。并针对学生如遇发热及身体不适等情况，立即启动应急预案，及时上报、隔离、消毒等应急措施落实进行重点演练。</p>\n<div class=\"contheight\">&nbsp;</div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/543.jpg\" alt=\"\" width=\"1080\" height=\"810\" /></div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/585.jpg\" alt=\"\" width=\"1080\" height=\"810\" /></div>\n<p>整场演练紧张有序，各环节密切配合，对发现的问题进行了反复推敲。演练结束后，召开了点评会，会上，对演练环节存在的不足进行总结，并提出相应的整改措施。</p>\n<div class=\"contheight\">&nbsp;</div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/626.jpg\" alt=\"\" width=\"1080\" height=\"810\" /></div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/666.jpg\" alt=\"\" width=\"1080\" height=\"729\" /></div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/685.jpg\" alt=\"\" width=\"1080\" height=\"729\" /></div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/715.jpg\" alt=\"\" width=\"293\" height=\"192\" /></div>\n<p>本次活动提升了各校应对新冠肺炎疫情的应急处置能力，检验了开学预案的有效性，明确了防疫流程中的规范程序，为做好2020年春季开学疫情防控工作打下坚实基础。</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>图/文：奇越</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>编辑：项民宇</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>审核：马东</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>中国加油！武汉加油！</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>我们一起加油！</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>推</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>荐</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>阅</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>读</p>\n<div class=\"contheight\">&nbsp;</div>\n<div style=\"text-align: center;\"><img src=\"https://imagepphcloud.thepaper.cn/pph/image/55/449/870.jpg\" alt=\"\" width=\"578\" height=\"328\" /></div>\n<p>&nbsp;</p>\n<div class=\"contheight\">&nbsp;</div>\n<p>原标题：《为开学复工做准备》</p>', 1599180287846, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (75, '中国各地精密智控疫情 复工开学保运营', '<div class=\"article-content\">\n<p><span class=\"bjh-p\"><span class=\"bjh-strong\">(抗击新冠肺炎)中国各地精密智控疫情 复工开学保运营</span></span></p>\n<p><span class=\"bjh-p\">中新社北京2月19日电 中国各地近期在全力复工复产、保障运营、开通网络就学就医时，积极采取各项措施，精密智控疫情。</span></p>\n<p><span class=\"bjh-p\">浙江省2月9日复工启动以来，截至近日没有发生因返工导致的新增确诊病例。浙江省防控办常务副主任、浙江省政府副秘书长陈广胜介绍，已正式开展对省内各设区市的精密智控指数评价，既要将重点区域、人员、场所管得更严密，又要把人流物流商流搞得更畅通。除温州外，浙江将取消省内高速公路、普通国省道以及农村公路的卡点，并简化复工复产确认程序。</span></p>\n<p><span class=\"bjh-p\">从浙江根据各县(市、区)疫情风险状况进行等级区分的&ldquo;疫情图&rdquo;来看，该省四分之三以上的县(市、区)已处于较低风险等级。</span></p>\n<p><span class=\"bjh-p\">四川省近日紧急安排预算内资金3903万元人民币，支持省疾病预防控制中心卫生监测检验中心建设，其中含P3实验室项目。该省还支持四川大学华西医院、四川省人民医院、成都医学院第一附属医院、西南医科大学附属医院、川北医学院附属医院5家省市级定点医院的设施改造，改造完成24间负压病房，并配置4辆负压救护车；支持四川大学华西医院谋划并启动建设精准诊断研发平台和疾病流行病学大数据研究平台。</span></p>\n<p><span class=\"bjh-p\">天津官方近日细化疫情防控工作，公布《关于保障和关爱新冠肺炎疫情防控一线医务人员的若干措施》，包括改善医务人员工作和休息条件、维护医务人员身心健康、落实医务人员待遇、提高卫生防疫津贴标准、给予职业发展激励、照顾子女入学等九方面十八条措施。强调对一线医务人员要加强人文关怀，创造更加安全的执业环境，弘扬职业精神做好表彰奖励。为一线医务人员办理一份保险额度不低于50万元的人身意外保险等关爱措施将陆续实施。</span></p>\n<p><span class=\"bjh-p\">云南锡业集团面对疫情，一手抓防控、一手保生产。1月份，云锡冶炼分公司锡、铅金属总产量完成月计划的106.77%，锡产量完成月计划的107.08%。截至2月16日，当期精锡产量已远超进度计划。此外，云锡集团重要的锡矿采选企业大屯锡矿锡氧化矿平均日出矿量为计划的115.7%，其他各项指标也实现百分之百超进度计划。截至2月16日，该公司采矿系统出矿量已超4000吨/日。</span></p>\n<p><span class=\"bjh-p\">同时，云锡锡材公司总经理白海龙介绍，2月份云锡锡材产量预计将不会低于平时的一半。此外，云锡锡化工公司工序也均已恢复正常生产，并于2月17日产出复工后的第一批次锡化工产品。</span></p>\n<p><span class=\"bjh-p\">内蒙古满洲里海关近日创新班列货物查验、换装等运作模式，实现中欧班列在该口岸实际通关时间保持在3小时以内。目前该海关与17个班列始发地海关、重要港口海关履行便利化通关协议，企业提前联系负责中欧班列验放的专岗关员，按约定时间办理通关手续，随到随放。</span></p>\n<p><span class=\"bjh-p\">今年1月份，从满洲里铁路口岸进境中欧班列62列，实现逆势同比增长1.6%，共载运5254个标准集装箱，同比增长4.9%。</span></p>\n<p><span class=\"bjh-p\">黑龙江省教育厅近日发布消息，在防控疫情期间，黑龙江省中小学实行&ldquo;停课不停学&rdquo;的网络教学，确保3月2日之后不具备开学条件的中小学校线上开课，并要求各地制定完善的线上教学工作方案，暂按4周时间制定教学计划。除小学低年级不作统一硬性要求外，其他年级均进行网络教学。在所有年级中，高中每天线上上课时间最长，但也不得超过3小时。且线上开课资源要对学生免费，产生的技术服务等费用由教育行政部门和学校承担。</span></p>\n<p><span class=\"bjh-p\">为抗击新冠肺炎疫情，黑龙江省医院还开通互联网医院线上图文咨询和视频问诊，近期又联合龙江广电网络集团公司开通电视端&ldquo;空中门诊&rdquo;健康服务专区，方便了对电视依赖比较大、不习惯操作智能手机的中老年人群体。黑龙江省医院健康管理中心主任王萍表示，目前&ldquo;电视医院&rdquo;开通的是发热门诊，随着系统进一步的优化，还将陆续上线更多临床科室。(完)(参与报道：张斌 王鹏 张道正 李爱平 王妮娜 史轶夫)</span></p>\n</div>', 1599180330873, 0, '未审核', 0, NULL, NULL, 3);
INSERT INTO `cms_article` VALUES (76, '全国累计确诊59804例！湖北继续延迟复工开学！中央空调能用吗？', '<div class=\"article-content\">\n<p><span class=\"bjh-p\">来源：国际金融报</span></p>\n<p><span class=\"bjh-p\">2月12日0&mdash;24时，31个省（自治区、直辖市）和新疆生产建设兵团报告新增确诊病例15152例（含湖北临床诊断病例13332例），重症病例减少174例，新增死亡病例254例（湖北242例，河南2例，天津、河北、辽宁、黑龙江、安徽、山东、广东、广西、海南、新疆生产建设兵团各1例），新增疑似病例2807例。</span></p>\n<p><span class=\"bjh-p\">当日新增治愈出院病例1171例，解除医学观察的密切接触者29429人。</span></p>\n<p><span class=\"bjh-p\">截至2月12日24时，据31个省（自治区、直辖市）和新疆生产建设兵团报告，现有确诊病例52526例（其中重症病例8030例），累计治愈出院病例5911例，累计死亡病例1367例，累计报告确诊病例59804例，现有疑似病例13435例。累计追踪到密切接触者471531人，尚在医学观察的密切接触者181386人。</span></p>\n<p><span class=\"bjh-p\">湖北新增确诊病例14840例（武汉13436例），新增治愈出院病例802例（武汉538例），新增死亡病例242例（武汉216例），现有确诊病例43455例（武汉30043例），其中重症病例7084例（武汉5426例）。累计治愈出院病例3441例（武汉1915例），累计死亡病例1310例（武汉1036例），累计确诊病例48206例（武汉32994例）。新增疑似病例1377例（武汉620例），现有疑似病例9028例（武汉4904例）。</span></p>\n<p><span class=\"bjh-p\">累计收到港澳台地区通报确诊病例78例：香港特别行政区50例（死亡1例，出院1例），澳门特别行政区10例（出院2例），台湾地区18例(出院1例)。</span></p>\n<p><span class=\"bjh-p\">湖北首次将临床诊断病例纳入新增确诊</span></p>\n<p><span class=\"bjh-p\">据悉，为做好新型冠状病毒肺炎患者早诊早治，落实好湖北病例应收尽收、应治尽治工作，按照《新型冠状病毒肺炎诊疗方案（试行第五版修正版）》，对湖北省以及湖北省以外其他省份的病例诊断标准进行了区分，湖北省增加了&ldquo;临床诊断病例&rdquo;分类，对疑似病例具有肺炎影像学特征者，确定为临床诊断病例，以便患者能及早按照确诊病例相关要求接受规范治疗，进一步提高救治成功率。</span></p>\n<p><span class=\"bjh-p\">目前，湖北省报告的13332例临床诊断病例纳入确诊病例统计，正加强病例救治，全力减少重症，降低病死率。</span></p>\n<p><span class=\"bjh-p\">钟南山、李兰娟院士团队分别从患者粪便样本中分离出新型冠状病毒</span></p>\n<p><span class=\"bjh-p\">新华社记者2月13日从科技部获悉，钟南山、李兰娟院士团队近日分别从新冠肺炎患者的粪便样本中分离出新型冠状病毒，这一发现证实了排出的粪便中的确存在活病毒，对于准确认识疾病的治病传播机理，帮助全社会有针对性地做好防控，切断疫情传播途径具有重要意义。</span></p>\n<div class=\"img-container\"><img class=\"normal\" src=\"https://pics1.baidu.com/feed/a6efce1b9d16fdfae5748c6a263eb85295ee7b55.jpeg?token=a1360ab1697ebed592a2082c2c92dc32&amp;s=F0A8F75A9673E7740B58D8010100A0C0\" width=\"453px\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<div class=\"img-container\"><img class=\"large\" src=\"https://pics7.baidu.com/feed/77094b36acaf2edd17587d8503a135ef3801936c.jpeg?token=29a132b37833d4bd8fb1678766d8020c&amp;s=630EB342BF8AAE0564E841060000A0C2\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<p><span class=\"bjh-p\">尽管目前还没有充分的证据说明存在粪口传播，但大家一定要勤洗手、注意个人卫生。两位院士团队的专家表示：目前正在对分离出的病毒进行进一步的传染病学研究和科研，大家不用恐慌，一定要更加重视个人和家庭的清洁，同时注意下水道通畅，避免有可能出现的粪便病毒的传播。</span></p>\n<div class=\"img-container\"><img class=\"large\" src=\"https://pics4.baidu.com/feed/7c1ed21b0ef41bd57ea3e326c16bb5cd3bdb3da3.jpeg?token=ce7f2eb237436ee8c610feec8ef2e57c&amp;s=ADA44B905E2556981E047CC70300E0B2\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\n<p><span class=\"bjh-p\">2月13日，国务院联防联控机制就重要医用物资保障和医疗资源调配保障最新进展情况举行发布会，集中回应公众关切。</span></p>\n<p><span class=\"bjh-p\">新冠肺炎主要传播途径尚未发生变化</span></p>\n<p><span class=\"bjh-p\">国家卫生健康委新闻发言人、宣传司副司长米锋：</span></p>\n<p><span class=\"bjh-p\">部分病例早期症状中，有腹泻等胃肠道症状，患者粪便标本中核酸检测阳性或分离到病毒，提示感染新冠病毒后，病毒在消化道内也可以增殖。通过对病例临床表现分析表明，不管患者消化道症状是初发症状，还是并发症状，该病最主要的靶器官仍为呼吸道，肺炎仍是最主要的临床表现。粪便中分离到病毒，并不意味着该病主要传播途径发生变化，仍为呼吸道、接触传播为主，消化道（粪-口）传播在全部传播中的作用和意义仍需进一步观察和研究。</span></p>\n<p><span class=\"bjh-p\">戴好口罩和做好手卫生，是最重要、最有效的防护措施。坚持勤洗手，规范洗手和手消毒，可以从根本上减少感染风险。在有疫情发生地区，医院、餐馆、商场等公共场所，需要做好常规的环境卫生和消毒工作。</span></p>\n<p><span class=\"bjh-p\">累计向湖北运抵医用防护服72.67万件</span></p>\n<p><span class=\"bjh-p\">工业和信息化部消费品工业司副司长曹学军：</span></p>\n<p><span class=\"bjh-p\">根据统计，截至2月12日，国内生产企业累计向湖北运抵医用防护服72.67万件，医用隔离面罩和隔离眼罩35.84万件，负压救护车156辆，呼吸机2286台，心电监护仪6929台，全自动红外测温仪761台。</span></p>\n<p><span class=\"bjh-p\">从目前看，最紧缺的还是医用防护服。为了保证供应，采取以下几个方面措施：</span></p>\n<p><span class=\"bjh-p\">一是加大力度增产扩能。</span></p>\n<p><span class=\"bjh-p\">二是精准做好供需对接。按照重点医疗物资需求和生产企业排产扩产的情况，加强供需对接匹配，科学安排调配计划，全力保障湖北疫情防控所需。根据疫情发展形势，对未来一段时间医疗物资需求进行滚动预测，据此倒排产量产能计划，努力实现疫情防控物资供应能力与实际需求相匹配，将稀缺物资用在最需要的地方。</span></p>\n<p><span class=\"bjh-p\">三是加强调拨全流程控制。</span></p>\n<p><span class=\"bjh-p\">下一步，对推动企业复工复产重点做好三方面的工作：</span></p>\n<p><span class=\"bjh-p\">一是分类分批推进企业复工复产。除湖北省外，其他地区在做好防控的同时，细化明确分批复工方案。优先组织医用防护服、口罩、消杀用品等重点防护物资生产企业复工复产扩产，推动全产业链企业复工复产。</span></p>\n<p><span class=\"bjh-p\">二是指导企业认真落实防疫防控各项工作要求。对于发现的疑似症状要就地隔离，及时送诊排查。健全日常清洁消杀，体温监测等制度，指导员工做好防护。</span></p>\n<p><span class=\"bjh-p\">三是加大对中小企业的帮扶力度。</span></p>\n<p><span class=\"bjh-p\">从目前看，根据工信部统计，全国共有消毒杀菌用品的企业563家，其中84消毒液生产企业171家，日产能5895吨，日产量达到了4597吨，开工率已经接近80%。手消毒液生产企业83家，日产能420吨，日产量205吨，开工率将近50%。医用酒精生产企业94家，日产能933吨，日产量906吨，开工率97.1%。这三类产品目前还都有库存，消杀用品的开工率正在逐步提高，应该说供给量满足需求是没有问题的。</span></p>\n<p><span class=\"bjh-p\">从短期看，消杀用品还存在着供需结构的矛盾，尽管从产量上能够满足全国的需求，但消杀生产企业主要是大包装的产品，目前各大电商平台、节后复工的企业、个体家庭对小包装的消杀产品需求量较大，在产品包装结构上存在着生产和市场需求衔接不够紧密的问题，再加之部分包材企业未复工，部分消杀产品面临包装材料供给不足的问题，特别是小包装的材料供给还存在一定的压力。</span></p>\n<p><span class=\"bjh-p\">下一步，将继督促尚未复工的包材企业尽快恢复生产，加强运输和包装材料、配件等方面的协调，随时掌握对湖北及其他地区的发货供应情况，确保疫情防控物资保障工作顺利开展。</span></p>\n<p><span class=\"bjh-p\">目前，医用防护服等重点医用物资仍然处于&ldquo;紧平衡&rdquo;状态，保供压力依然较大。工信部于1月26日晚启动了驻企特派员的工作。目前，已经陆续选派驻企特派员60名，赴13个省市、41家重点骨干企业驻厂。驻企特派员机制是突发疫情状态下的临时应急举措，待疫情缓解将适时退出。</span></p>\n<p><span class=\"bjh-p\">全国口罩产能利用率已达94%</span></p>\n<p><span class=\"bjh-p\">国家发展改革委产业发展司一级巡视员夏农：</span></p>\n<p><span class=\"bjh-p\">2月2日以来，我国口罩产量逐日提升。截至2月11日，全国口罩产能利用率已经达到94%；特别是一线防控急需的医用N95口罩，产能利用率已达到128%，有8个省份达到或超过100%；医用的非N95口罩的产能利用率达到了106%，有10个省份达到或超过了100%。</span></p>\n<p><span class=\"bjh-p\">在确诊病例超过千人的重点省份中，本省内医用N95口罩的产量都有了不同程度的增长。湖北的日产量从2月2日的4.5万只大幅增长到2月11日的15.8万只，广东的日产量从3.3万只增加到了4万只，河南、浙江从没有产量分别增长到1.4万只、10万只，对这些省份的疫情防控发挥了重要作用。通过全国统一调度，已经能够保障一线医护人员的医用口罩防护需要。</span></p>\n<p><span class=\"bjh-p\">下一步，将继续抓好口罩生产和扩能等相关工作。一是进一步加强生产监测和调度，及时解决企业生产中存在的问题，在保障安全生产和产品质量的前提下，力争全面达产、超产。二是进一步扩大医用，特别是医用N95口罩生产。三是进一步加强关键设备和原辅材料的供应协调，保障项目建设实施和顺利达产。同时，对重点省份和行业，还将通过调配使用等方式切实保障重点需求。</span></p>\n<p><span class=\"bjh-p\">目前，有些口罩企业还没有满负荷生产，希望这些企业抓紧释放产能，尽快实现满负荷生产，有条件的企业可以积极实施技术改造转产、扩产。最近，相关部门、地方出台了技改、财税、金融、收储等一系列支持政策措施，有些已经落实，有些正在落实之中。请相关企业放下包袱、全力以赴，开足马力生产。希望通过各方面共同努力，有效缓解口罩供求矛盾的问题。</span></p>\n<p><span class=\"bjh-p\">调集22支国家紧急医学救援队、3个移动P3实验室赶赴武汉</span></p>\n<p><span class=\"bjh-p\">国家卫生健康委医政医管局监察专员郭燕红：</span></p>\n<p><span class=\"bjh-p\">目前，全国29个省自治区直辖市和新疆生产建设兵团调集了近2万多医护人员，组成了180多个医疗队支持湖北和武汉。今天，军队系统又增援了2600名医护赶赴武汉承担救治任务。增派的医疗力量当中很多都是重症专业的医护人员，有7000多名，他们主要是帮助武汉，包括武汉以外的相关地市加大重症救治力度，能够有效地降低病死率，提高治愈率。还调集了22支国家紧急医学救援队伍赶赴武汉，这也是顶级力量，帮助武汉展开方舱医院收治更多患者，还从全国调集了3个移动P3实验室赶赴武汉，举全国之力支援武汉，支援湖北。</span></p>\n<p><span class=\"bjh-p\">除了武汉以外，16个地市、县市州也同样是湖北省救治工作、防控工作非常重要的一个方面。采用省包地市的对口支援关系，按照以省包市责任包干，选派江苏、广东、山东、浙江等19个省，以省来支持地市或县级市的医疗救治工作。目前已派出一共25支医疗队，3500多名医务人员，实现对16个地市需求的全覆盖和全满足。并要求根据地市的需要，在派出力量的同时，还要增派50%的力量作为储备。</span></p>\n<p><span class=\"bjh-p\">从2月7日开始，每天治愈出院的患者都达到500以上，目前累计治愈出院病例近6000例。专家对治愈出院病人当中的597例进行初步分析，在出院病人当中，90%左右是诊疗方案当中的轻型和普通型，10%是诊疗方案当中的重症和危重症。这些病例平均住院日10天左右，92%的患者是早期使用抗病毒治疗，或者是抗病毒治疗联合对症治疗，积极的呼吸支持、循环支持，效果也是非常好的。</span></p>\n<p><span class=\"bjh-p\">专家：大部分中央空调可以用</span></p>\n<p><span class=\"bjh-p\">中国疾控中心环境所研究员张流波：</span></p>\n<p><span class=\"bjh-p\">集中空调在使用之前应该了解它的类型、新风口卫生状况、供风范围，这些情况了解清楚了，大部分的中央空调是可以用的。二是中央空调有一部分是全空气系统的，全空气系统的中央空调在使用时要求关闭回风，用全新风来运行。三是还有一些风机盘管加新风系统的，这是我国最主要的一款集中空调的类型，这种类型的集中空调，要求各个房间之间的空气应该是独立的，在独立的情况下，这种风机盘管加新风系统的空调是可以用的，用的时候应该尽量开最大的新风。</span></p>\n<p><span class=\"bjh-p\">除此之外，还要特别注意的是，有一些集中空调是有消毒装置或者是有净化装置的，在疫情流行期间应该打开运行。每周还要对出风口、回风口的滤器等部位进行清洗，这也是很关键的。如果在供风的范围内发现有新冠肺炎的病人或疑似病人，应该及时关闭中央空调，进行消毒，再评估合格以后再使用。</span></p>\n<p><span class=\"bjh-p\">一些企业、商场甚至有交通要道布置一个消毒通道，人员经过通道时，要对全身喷消毒剂或者做消毒处理以后再放行。这种措施是多余的，也是起不到切断传播途径的作用的，而且是有害的，应该避免。</span></p>\n<p><span class=\"bjh-p\">所有消毒剂都是有效的。目前未推荐用洗必泰消毒，相关的试验也正在做。</span></p>\n<p><span class=\"bjh-p\">湖北：继续延迟复工开学</span></p>\n<p><span class=\"bjh-p\">2月13日，湖北省新型冠状病毒感染肺炎疫情防控指挥部通告：继续延迟复工开学。</span></p>\n<p><span class=\"bjh-p\">为进一步加强疫情防控工作，有效减少人员流动聚集，阻断疫情传播，切实保障人民群众生命安全和身体健康，根据《中华人民共和国突发事件应对法》《中华人民共和国传染病防治法》和湖北省重大突发公共卫生事件I级响应机制的有关规定，现就延迟企业复工和学校开学有关事项通告如下：</span></p>\n<p><span class=\"bjh-p\">一、省内各类企业不早于2月20日24时前复工。涉及疫情防控必需（医疗器械、药品、防护品生产和销售等行业）、保障公共事业运行必需（供水、供电、油气、通讯等行业）、群众生活必需（超市卖场、食品生产和供应等行业）及其他涉及重要国计民生的相关企业除外。复工企业要严格落实各项疫情防控措施，依法保障劳动者合法权益。各行业主管部门要加强对企业防疫工作的指导和监督。</span></p>\n<p><span class=\"bjh-p\">二、省内大专院校、中小学、中职学校、技工院校、幼儿园延期开学。具体开学时间，将根据疫情防控情况，经科学评估后确定，并提前向社会公布。</span></p>\n<p><span class=\"bjh-p\">三、各级党政机关、事业单位要组织干部职工下沉到村（社区）基层一线，参与疫情防控工作。</span></p>\n<p><span class=\"bjh-p\">四、广大居民（包括在湖北探亲访友休假的外地人员）应严格遵守现居住地疫情防控要求，尽量减少出行，不参加集聚性活动，做好居家环境卫生。出入公共场所必须佩戴口罩，对不听劝阻的人员依据相关法律法规予以处理。</span></p>\n<p><span class=\"bjh-p\">五、各地各单位要认真落实本通告要求，强化主体责任，切实把各项疫情防控和服务保障措施抓实、抓细、抓落地，确保社会大局平稳有序。</span></p>\n<p><span class=\"bjh-p\">来源：人民日报、新华社、国家卫健委官方网站等</span></p>\n</div>', 1599180362416, 0, '未审核', 0, NULL, NULL, 3);
COMMIT;

-- ----------------------------
-- Table structure for cms_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_category`;
CREATE TABLE `cms_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `no` int(11) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_category` (`parent_id`),
  CONSTRAINT `fk_category_category` FOREIGN KEY (`parent_id`) REFERENCES `cms_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cms_category
-- ----------------------------
BEGIN;
INSERT INTO `cms_category` VALUES (3, '最新进展', '疫情的动态', 1, NULL);
INSERT INTO `cms_category` VALUES (4, '公告', NULL, 2, NULL);
INSERT INTO `cms_category` VALUES (5, '学术活动', NULL, 3, NULL);
INSERT INTO `cms_category` VALUES (6, '媒体聚焦', NULL, 4, NULL);
INSERT INTO `cms_category` VALUES (7, '学院快讯', NULL, 5, NULL);
INSERT INTO `cms_category` VALUES (9, '学术进展', '冰棺', 6, NULL);
COMMIT;

-- ----------------------------
-- Table structure for cms_comment
-- ----------------------------
DROP TABLE IF EXISTS `cms_comment`;
CREATE TABLE `cms_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text,
  `comment_time` bigint(20) DEFAULT NULL,
  `floor` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `article_id` bigint(20) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_user` (`user_id`),
  KEY `fk_comment_article` (`article_id`),
  KEY `fk_comment_comment` (`parent_id`),
  CONSTRAINT `fk_comment_article` FOREIGN KEY (`article_id`) REFERENCES `cms_article` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_comment_comment` FOREIGN KEY (`parent_id`) REFERENCES `cms_comment` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk_comment_user` FOREIGN KEY (`user_id`) REFERENCES `base_user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cms_comment
-- ----------------------------
BEGIN;
INSERT INTO `cms_comment` VALUES (1, '作者厉害', 1597887845133, 1, NULL, 2, 31, NULL);
INSERT INTO `cms_comment` VALUES (2, '作者牛皮', 1597887856033, 1, NULL, 5, 31, 1);
INSERT INTO `cms_comment` VALUES (3, 'springmvc到底怎么用？', 1597887861133, 1, NULL, 3, 2, NULL);
INSERT INTO `cms_comment` VALUES (4, 'xixi', 1597887866133, 1, NULL, 3, 31, 2);
INSERT INTO `cms_comment` VALUES (5, '还不错', 1597887886133, 2, NULL, 5, 31, NULL);
INSERT INTO `cms_comment` VALUES (6, '这个项目很精彩', 1598238866985, 3, NULL, 1, 31, NULL);
INSERT INTO `cms_comment` VALUES (7, '是的', 1598238933429, 3, NULL, 2, 31, 6);
INSERT INTO `cms_comment` VALUES (8, 'just so so', 1598321920796, 3, NULL, 3, 31, 6);
INSERT INTO `cms_comment` VALUES (9, '111', 1598335535982, 4, NULL, 1, 31, NULL);
INSERT INTO `cms_comment` VALUES (13, '哈哈(ಡωಡ)hiahiahia', 1598596654954, 1, NULL, 5, 48, NULL);
COMMIT;

-- ----------------------------
-- Table structure for visual_epidemic
-- ----------------------------
DROP TABLE IF EXISTS `visual_epidemic`;
CREATE TABLE `visual_epidemic` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `confirmed` bigint(20) DEFAULT NULL,
  `suspected` bigint(20) DEFAULT NULL,
  `dead` bigint(20) DEFAULT NULL,
  `cure` bigint(20) DEFAULT NULL,
  `severe` bigint(20) DEFAULT NULL,
  `outside` bigint(20) DEFAULT NULL,
  `input_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of visual_epidemic
-- ----------------------------
BEGIN;
INSERT INTO `visual_epidemic` VALUES (1, '中国', NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, 1599004942749);
INSERT INTO `visual_epidemic` VALUES (2, '中国', '江苏省', '苏州市', '昆山市', 8, NULL, NULL, NULL, NULL, NULL, 1599004942749);
INSERT INTO `visual_epidemic` VALUES (3, '中国', '上海市', '市辖区', '黄浦区', 2, 1, 2, 1, 2, 1, 1599013923262);
INSERT INTO `visual_epidemic` VALUES (4, '中国', '上海市', '市辖区', '黄浦区', 1, 2, 3, 1, 1, 1, 1599035918251);
INSERT INTO `visual_epidemic` VALUES (5, '中国', '上海市', '市辖区', '黄浦区', 2, 0, 0, 0, 0, 2, 1599098172714);
COMMIT;

-- ----------------------------
-- Table structure for visual_hospital
-- ----------------------------
DROP TABLE IF EXISTS `visual_hospital`;
CREATE TABLE `visual_hospital` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  `flags` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of visual_hospital
-- ----------------------------
BEGIN;
INSERT INTO `visual_hospital` VALUES (4, '会昌县人民医院', '县医院', '111', '江西省', '萍乡市', '', NULL, NULL, '', '115.793333', '25.600478');
INSERT INTO `visual_hospital` VALUES (5, '南昌大学第二附属眼科医院', '南昌市一流眼科医院', '', '江西省', '南昌市', '', NULL, NULL, '', '115.90363', '28.684013');
INSERT INTO `visual_hospital` VALUES (6, '南昌县人民医院', '565', '2', '江西省', '南昌市', NULL, NULL, NULL, NULL, '115.94226', '28.544208');
INSERT INTO `visual_hospital` VALUES (8, '湖口人民第一人民医院', '1111', '12.2731', '江西省', '九江市', '湖口县', NULL, NULL, '111', '14.23412', '42.1372');
INSERT INTO `visual_hospital` VALUES (9, '湖口第二人民医院', '1123123', '41231', '江西省', '九江市', '湖口县', NULL, NULL, '327183', '23.4527', '32.3126');
INSERT INTO `visual_hospital` VALUES (10, '江西省九江市湖口县第二人民医院', '好医院', '2311321', '江西省', '九江市', '湖口县', NULL, NULL, '12131', '23.3214', '12.3141');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
