package com.briup.visual.dao;

import com.briup.visual.bean.Hospital;
import com.briup.visual.bean.HospitalExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface HospitalMapper {
    long countByExample(HospitalExample paramHospitalExample);

    int deleteByExample(HospitalExample paramHospitalExample);

    int deleteByPrimaryKey(Long paramLong);

    int insert(Hospital paramHospital);

    int insertSelective(Hospital paramHospital);

    List<Hospital> selectByExample(HospitalExample paramHospitalExample);

    Hospital selectByPrimaryKey(Long paramLong);

    int updateByExampleSelective(@Param("record") Hospital paramHospital, @Param("example") HospitalExample paramHospitalExample);

    int updateByExample(@Param("record") Hospital paramHospital, @Param("example") HospitalExample paramHospitalExample);

    int updateByPrimaryKeySelective(Hospital paramHospital);

    int updateByPrimaryKey(Hospital paramHospital);
}


